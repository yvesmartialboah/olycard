<?php

namespace Database\Seeders;

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Boah',
            'email' => 'yvesmartialboah@gmail.com',
            'password' => Hash::make('IAmTheAdmin@'), // password
            'isvalid' => true,
            'prenom' => 'Yves Martial',
            'contact_1' => '+2250757869730',
            'contact_2' => '+2250544344406',
            'status' => 'coveradmin', // Super Admin
            'image_de_profil' => 'https://lh3.googleusercontent.com/a-/AOh14Gjwr0QaRqByYveS5kwiNAxpg7uvuvhj3e889CeYrg=s96-c',
            'image_de_fond' => null,
            'link_linkedin' => 'https://ci.linkedin.com/in/yves-martial-boah',
            'link_youtube' => 'https://www.youtube.com/@yvesmartialboah',
            'link_facebook' => 'https://www.facebook.com/yvesmartial.boah.7/',
            'site_internet' => 'https://yvesboah.netlify.app/',
            'localisation_name' => 'Plateau Immeuble Trade Center',
            'latitude' => 5.325277,
            'longitude' => -4.0184761,
            'date_de_naissance' => null,
            'fonction' => 'Ingenieur Développeur',
            'entreprise' => 'GS2E',
            'email_verified_at' => now(),
            'remember_token' => Str::random(10),
            'slug' => Str::slug('yves martial'),
            'description' => 'Eos voluptatibus veniam debitis ratione itaque et cum. Aliquam qui dicta ut optio. Natus dolorem perferendis esse quia quam et nihil. Odio labore consequatur id magni esse vel sed illum.',

        ]);

        DB::table('users')->insert([
            'name' => 'Zouzoua',
            'email' => 'dev@gmail.com',
            'password' => Hash::make('Bill@2020'), // password
            'isvalid' => true,
            'prenom' => 'Essis Cédric',
            'contact_1' => '+2250103772742',
            'contact_2' => null,
            'status' => 'coveradmin', // Super Admin
            'image_de_profil' => null,
            'image_de_fond' => null,
            'link_linkedin' => null,
            'link_facebook' => null,
            'link_youtube' => null,
            'site_internet' => null,
            'localisation_name' => null,
            'latitude' => null,
            'longitude' => null,
            'date_de_naissance' => null,
            'fonction' => 'Ingenieur Développeur',
            'entreprise' => 'AIRPORT CI',
            'email_verified_at' => now(),
            'remember_token' => Str::random(10),
            'slug' => Str::slug('cedric zouzoua'),
            'description' => 'Eos voluptatibus veniam debitis ratione itaque et cum. Aliquam qui dicta ut optio. Natus dolorem perferendis esse quia quam et nihil. Odio labore consequatur id magni esse vel sed illum.',
        ]);
    }
}
