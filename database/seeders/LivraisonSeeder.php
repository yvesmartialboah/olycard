<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class LivraisonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('livraisons')->insert([
            'libelle' => 'Livraison de carte',
            'adresse' => 'Nouveau quartier',
            'nomprenoms' => 'Jean Fabrice',
            'telephone' =>  "0103772742",
            'datelivraison' => Carbon::now()->format('Y-m-d'),
            'statut' => 1, // 0 => encours | 1 => Livré | 2 => echec livraison
            'commande_id' => 2
        ]);
    }
}
