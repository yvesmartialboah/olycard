<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Seeders\PaysSeeder;
use Database\Seeders\AdminSeeder;
use Database\Seeders\VilleSeeder;
use Database\Seeders\CommandeSeeder;
use Database\Seeders\LivraisonSeeder;
use Database\Seeders\RendezvousSeeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(10)->create();
        $this->call([
            UserSeeder::class,
            AdminSeeder::class,
            PaysSeeder::class,
            VilleSeeder::class,
            CommandeSeeder::class,
            RendezvousSeeder::class,
            LivraisonSeeder::class,
        ]);
    }
}
