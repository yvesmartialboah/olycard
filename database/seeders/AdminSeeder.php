<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            'name'=>'Essis Cedric',
            'email' => 'zouzuacedric17@gmail.com',
            'password' => Hash::make('Can@2023'),
        ]);

        DB::table('admins')->insert([
            'name' => 'Boah',
            'email' => 'yvesmartialboah@gmail.com',
            'password' => Hash::make('Caf@2023'),
        ]);
    }
}
