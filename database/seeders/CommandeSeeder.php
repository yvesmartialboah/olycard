<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class CommandeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('commandes')->insert([
            'libelle' => "Commande de carte",
            'societe' => "TINITZ",
            'datelivraison' =>  Carbon::now()->format('Y-m-d'),
            'adressecoplementaire' =>  "Abidjan Yopougon",
            'quantite' => 2,
            'statut' => 0, // 0 => en attente | 1 => validé | 2 => rejeté
            'pays_id' => 1,
            'ville_id' => 1,
            'user_id' => 1,
        ]);

        DB::table('commandes')->insert([
            'libelle' => "Commande de carte Simple",
            'societe' => "Aucune",
            'datelivraison' =>  Carbon::now()->format('Y-m-d'),
            'adressecoplementaire' =>  "Abidjan Koumassi",
            'quantite' => 1,
            'statut' => 1, // 0 => en attente | 1 => validé | 2 => rejeté
            'pays_id' => 1,
            'ville_id' => 1,
            'user_id' => 1,
        ]);
    }
}
