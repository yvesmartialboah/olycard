<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class RendezvousSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rendezvouses')->insert([
            'reference' => 'Dkf47fkzm',
            'nom' => 'Zouzoua',
            'prenom' => 'Essis Cedric',
            'telephone' =>'0201425606',
            'email' => 'zouzouacedric17@gmail.com',
            'daterendezvous' => Carbon::now()->format('Y-m-d'),
            'statut' => 1, // 1=> effectué | 2 annulé
        ]);
    }
}
