<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'password' => Hash::make('12345678'), // password
            'isvalid' => true,
            'prenom' => $this->faker->name(),
            'contact_1' => $this->faker->phoneNumber(),
            'contact_2' => null,
            'status' => 'client', // Client
            'image_de_profil' => $this->faker->imageUrl(),
            'image_de_fond' => null,
            'link_linkedin' => null,
            'link_facebook' => null,
            'link_youtube' => null,
            'site_internet' => null,
            'localisation_name' => null,
            'latitude' => null,
            'longitude' => null,
            'date_de_naissance' => null,
            'remember_token' => Str::random(10),
            'fonction' => $this->faker->jobTitle(),
            'entreprise' => $this->faker->company(),
            'slug' => Str::slug($this->faker->name().''.$this->faker->name()),
            // 'slug' => $this->faker->slug(),
            'description' => $this->faker->text()
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}
