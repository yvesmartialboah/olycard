<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->boolean('isvalid')->default(false); // Dit si la page du client doit être affichée ou pas (Par défaut "NON")
            $table->string('prenom')->nullable();
            $table->string('fonction')->nullable();
            $table->string('entreprise')->nullable();
            $table->string('contact_1')->unique()->nullable();
            $table->string('contact_2')->nullable(); // Autre contact
            $table->string('status')->default('client'); // client && coveradmin
            $table->longText('image_de_profil')->nullable();
            $table->longText('image_de_fond')->nullable();
            $table->string('link_linkedin')->nullable();
            $table->string('link_facebook')->nullable();
            $table->string('link_youtube')->nullable();
            $table->string('site_internet')->nullable();
            $table->string('localisation_name')->nullable();
            $table->double('longitude',15,8)->nullable();
            $table->double('latitude',15,8)->nullable();
            $table->date('date_de_naissance')->nullable();
            $table->string('slug')->unique();
            $table->string('description')->nullable();
            $table->string('imageqrcode')->nullable();

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
