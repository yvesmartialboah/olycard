@extends('themes.template')
@section('content')
    {{-- @include('themes.partials.commandeform') --}}

    @push('css')
        <style>
            #formulaire {
                float: left;
                width: 50%;
                padding: 20px;
            }

            #carte {
                float: left;
                width: 50%;
                padding: 20px;
            }

            #carte-de-visite {
                max-width: 100%;
                height: auto;
            }
        </style>
    @endpush
    <div id="formulaire">
        <h2>Formulaire</h2>
        <form id="myForm">
            <label for="nom">Nom:</label>
            <input type="text" id="nom" name="nom"><br><br>
            <label for="prenom">Prénom:</label>
            <input type="text" id="prenom" name="prenom"><br><br>
            <label for="entreprise">Entreprise:</label>
            <input type="text" id="entreprise" name="entreprise"><br><br>
            <button type="button" onclick="updateCarte()">Afficher sur la carte de visite</button>
        </form>
    </div>

    <div id="carte">
        <h2>Carte de Visite</h2>
        <img id="carte-de-visite" src="carte_de_visite_vide.png" alt="Carte de visite">
    </div>
    @push('js')
        <script>
            function updateCarte() {
                var nom = document.getElementById("nom").value;
                var prenom = document.getElementById("prenom").value;
                var entreprise = document.getElementById("entreprise").value;

                var carteDeVisite = document.getElementById("carte-de-visite");
                carteDeVisite.src =
                "carte_de_visite_template.png"; // Assurez-vous d'avoir une image de carte de visite template

                // Créez une balise <p> pour chaque information et ajoutez-la à la carte de visite
                var nomElement = document.createElement("p");
                nomElement.textContent = "Nom: " + nom;
                carteDeVisite.parentNode.insertBefore(nomElement, carteDeVisite.nextSibling);

                var prenomElement = document.createElement("p");
                prenomElement.textContent = "Prénom: " + prenom;
                carteDeVisite.parentNode.insertBefore(prenomElement, carteDeVisite.nextSibling);

                var entrepriseElement = document.createElement("p");
                entrepriseElement.textContent = "Entreprise: " + entreprise;
                carteDeVisite.parentNode.insertBefore(entrepriseElement, carteDeVisite.nextSibling);
            }
        </script>
    @endpush
    </body>

    </html>
@endsection
