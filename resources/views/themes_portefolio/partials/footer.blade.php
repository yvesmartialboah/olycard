<footer id="colophon" class="site-footer" role="contentinfo">
    <div class="site-info">
        <div class="textwidget">
            <p><a href="https://olycard.ci/">Copyright &copy; 2024 by OlyCard</a></p>
        </div>
    </div>
</footer>
