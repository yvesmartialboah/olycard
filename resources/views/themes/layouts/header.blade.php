    <header class="header-section bgadd ">
        <div class="container">
            <div class="header-wrapper">
                <div class="logo-menu">
                    <a class="{{ $menu == 'accueil' ? 'active' : '' }}" href="{{ route('home') }}" class="logo">
                        <img src="{{ asset('assets/img/logo/logo.png') }}" alt="logo">
                    </a>
                    <a href="{{ route('home') }}" class="small__logo d-xl-none">
                        <img src="{{ asset('assets/img/logo/logo.png') }}" alt="logo">
                    </a>
                </div>
                <ul class="main-menu">
                    <li>
                        <a class="{{ $menu == 'offres' ? 'active' : '' }}" href="{{ route('offres') }}">
                            Nos offres
                        </a>
                    </li>
                    {{-- <li>
                        <a href="#">
                            Solutions
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Ressource
                        </a>
                    </li> --}}
                </ul>
                <div class="menu__right__components d-flex align-items-center">
                    <div class="menu__components">
                        @if (auth()->user())
                            <ul class="main-menu">
                                <li>
                                    <a href="javascript:void(0)" class="fz-24">
                                        <img src="{{ asset('assets/img/profile-user.png') }}" width="40">
                                        <i class="fas fa-chevron-down"></i>
                                    </a>
                                    <ul class="sub-menu">
                                        <li>
                                            <a href="{{ route('users.identity', auth()->user()->slug) }}">Profil en
                                                ligne</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('users.logout') }}">Deconnexion</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        @else
                            <a href="{{ url('/login-users') }}" class="cmn--btn text-white">
                                Connexion
                            </a>
                        @endif
                    </div>
                    <div class="header-bar d-lg-none">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
        </div>
    </header>
