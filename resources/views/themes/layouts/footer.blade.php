    <footer class="footer__section bgadd">
        <div class="container">
            <div class="footer__top pt-120 pb-120">
                <div class="row g-4">
                    <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6">
                        <div class="footer__item">
                            <a href="index.html" class="footer__logo mb-24 d-block">
                                <img src="assets/img/logo/logo.png" alt="logo">
                            </a>
                            <p class="pfz-16 inter fw-400 text-white mb-30">
                                Are you looking for a credit card that fits your needs? Our credit card market place
                                makes it easy to find the perfect card.
                            </p>
                            <ul class="social d-flex align-items-center">
                                <li>
                                    <a href="javascript:void(0)">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">
                                        <i class="fab fa-instagram"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">
                                        <i class="fab fa-linkedin-in"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">
                                        <i class="fab fa-vimeo-v"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xxl-2 col-xl-2 col-lg-2 col-md-6 col-sm-6">
                        <div class="footer__item">
                            <a href="javascript:void(0)"
                                class="footer__title fz-24 fw-600 inter text-white mb-24 d-block">
                                Quick Link
                            </a>
                            <ul class="quick__link">
                                <li>
                                    <a href="about.html" class="fz-16 fw-400 inter text-white d-block">
                                        About us
                                    </a>
                                </li>
                                <li>
                                    <a href="featured.html" class="fz-16 fw-400 inter text-white d-block">
                                        Categories
                                    </a>
                                </li>
                                <li>
                                    <a href="compare-card.html" class="fz-16 fw-400 inter text-white d-block">
                                        Compare Cards
                                    </a>
                                </li>
                                <li>
                                    <a href="faqs.html" class="fz-16 fw-400 inter text-white d-block">
                                        FAQs
                                    </a>
                                </li>
                                <li>
                                    <a href="blog.html" class="fz-16 fw-400 inter text-white d-block">
                                        Blog
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6">
                        <div class="footer__item">
                            <a href="javascript:void(0)"
                                class="footer__title fz-24 fw-600 inter text-white mb-24 d-block">
                                Contact
                            </a>
                            <ul class="footer__contact">
                                <li>
                                    <a href="javascript:void(0)"
                                        class="fz-16 d-flex align-items-center gap-3 fw-400 inter text-white d-block">
                                        <i class="material-symbols-outlined cmn__icon">
                                            phone_in_talk
                                        </i>
                                        <span>
                                            (316) 555-0116
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"
                                        class="fz-16 d-flex align-items-center gap-3 fw-400 inter text-white d-block">
                                        <i class="material-symbols-outlined cmn__icon">
                                            mark_as_unread
                                        </i>
                                        <span>
                                            <span class="__cf_email__"
                                                data-cfemail="3b52555d547b5e435a564b575e15585456">[email&#160;protected]</span>
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"
                                        class="fz-16 d-flex align-items-center gap-3 fw-400 inter text-white d-block">
                                        <I class="material-symbols-outlined cmn__icon">
                                            distance
                                        </I>
                                        <span>
                                            31 Brandy Way, Sutton, SM2 6SE
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6">
                        <div class="footer__item">
                            <a href="javascript:void(0)"
                                class="footer__title fz-24 fw-600 inter text-white mb-24 d-block">
                                Newsletter
                            </a>
                            <p class="pfz-16 fw-400 inter mb-24">
                                Subscribe our newsletter to get our latest update & adress
                            </p>
                            <form action="#0" class="d-flex align-items-center">
                                <input type="text" placeholder="Email address">
                                <button type="submit" class="cmn--btn">
                                    <span>
                                        <i class="material-symbols-outlined">
                                            send
                                        </i>
                                    </span>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer__bottom d-flex align-items-center">
                <p class="fz-16 fw-400 inter text-white">
                    Copyright &copy; 2023 <a href="javascript:void(0)" class="base2">Cardzone.</a> Designed By <a
                        href="https://themeforest.net/user/pixelaxis" class="base3">Pixelaxis</a>
                </p>
                <ul class="help__support d-flex align-items-center">
                    <li>
                        <a href="javascript:void(0)" class="text-white fz-16 fw-400 inter">
                            Help & Support
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" class="text-white fz-16 fw-400 inter">
                            Privacy policy
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" class="text-white fz-16 fw-400 inter">
                            Terms & Conditions
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </footer>
