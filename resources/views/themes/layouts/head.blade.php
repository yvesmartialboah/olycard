<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ env('APP_NAME') }} - {{ $title ?? '' }}</title>
    <!--Favicon img-->
    <link rel="shortcut icon" href="{{ asset('assets/img/logo/logo.png') }}">
    <!--Bootstarp min css-->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <!--Bootstarp map css-->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.css.map') }}">
    <!--Odometer css-->
    <link rel="stylesheet" href="{{ asset('assets/css/odometer.css') }}">
    <!--Animate css-->
    <link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}">
    <!--Magnifiq Popup css-->
    <link rel="stylesheet" href="{{ asset('assets/css/magnific-popup.css') }}">
    <!--All min css-->
    <link rel="stylesheet" href="{{ asset('assets/css/all.min.css') }}">
    <!--Owl Carousel min css-->
    <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.min.css') }}">
    <!--Owl Carousel theme css-->
    <link rel="stylesheet" href="{{ asset('assets/css/owl.theme.default.css') }}">
    <!--Owl Nice select css-->
    <link rel="stylesheet" href="{{ asset('assets/css/nice-select.css') }}">
    <!--Glyphter icon css-->
    <link rel="stylesheet" href="{{ asset('assets/css/google-font.css') }}">
    <!--Date Ranger css-->
    <link rel="stylesheet" href="{{ asset('assets/css/daterangepicker.css') }}">
    <!--main css-->
    <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
    @stack('css')
</head>
