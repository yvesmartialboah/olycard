<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from pixner.net/cardzone/cardzone/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 10 Dec 2023 22:24:28 GMT -->

@include('themes.layouts.head')

<body>


    <!-- Preloader End Here -->

    <!-- Header Here -->
    @include('themes.layouts.header')
    <!-- Header End -->
    @yield('content')
    <!-- Hero Section Here -->
    @include('themes.layouts.footer')
    <!-- Hero Section End -->


    <!--Search Popup-->
    <div id="searchPopup" class="search__popup">
        <form class="popup-content d-flex align-items-center">
            <input type="text" placeholder="Search Here">
            <button id="closeButton">
                <i class="material-symbols-outlined">
                    close
                </i>
            </button>
        </form>
    </div>
    <!--Search Popup-->

    <!--Jquery 3 6 0 Min Js-->
    {{-- <script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script> --}}
    <script src="{{ asset('assets/js/jquery-3.7.0.min.js') }}"></script>
    <!--Bootstrap bundle Js-->
    <script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
    <!--Viewport Jquery Js-->
    <script src="{{ asset('assets/js/viewport.jquery.js') }}"></script>
    <!--Odometer min Js-->
    <script src="{{ asset('assets/js/odometer.min.js') }}"></script>
    <!--Magnifiw Popup Js-->
    <script src="{{ asset('assets/js/jquery.magnific-popup.min.js') }}"></script>
    <!--Counddown min pkgd Js-->
    <script src="{{ asset('assets/js/countdown.min.js') }}"></script>
    <!--Wow min Js-->
    <script src="{{ asset('assets/js/wow.min.js') }}"></script>
    <!--Owl carousel min Js-->
    <script src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
    <!--moment min js-->
    <script src="{{ asset('assets/js/moment.min.js') }}"></script>
    <!--daterangepicker-->
    <script src="{{ asset('assets/js/daterangepicker.min.js') }}"></script>
    <!--Owl nice select Js-->
    <script src="{{ asset('assets/js/jquery.nice-select.min.js') }}"></script>
    <!--main Js-->
    <script src="{{ asset('assets/js/main.js') }}"></script>
    @stack('js')
</body>


<!-- Mirrored from pixner.net/cardzone/cardzone/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 10 Dec 2023 22:24:50 GMT -->

</html>
