<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulaire et Carte de Visite</title>
    <!-- Intégration de Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">
    <style>
        /* Ajoutez ici votre propre style personnalisé si nécessaire */
        #carte-de-visite-container {
            position: relative;
        }

        #carte-de-visite-texte {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            background-color: rgba(255, 255, 255, 0.7);
            /* Fond semi-transparent */
            padding: 10px;
        }

        #carte-de-visite {
            max-width: 100%;
            height: auto;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2>Formulaire</h2>
                <form id="myForm">
                    <div class="mb-3">
                        <label for="nom" class="form-label">Nom:</label>
                        <input type="text" class="form-control" id="nom" name="nom">
                    </div>
                    <div class="mb-3">
                        <label for="prenom" class="form-label">Prénom:</label>
                        <input type="text" class="form-control" id="prenom" name="prenom">
                    </div>
                    <div class="mb-3">
                        <label for="entreprise" class="form-label">Entreprise:</label>
                        <input type="text" class="form-control" id="entreprise" name="entreprise">
                    </div>
                    <button type="button" class="btn btn-primary" onclick="updateCarte()">Afficher sur la carte de
                        visite</button>
                </form>
            </div>
            <div class="col-md-6">
                <h2>Carte de Visite</h2>
                <div id="carte-de-visite-container">
                    <img id="carte-de-visite" src="{{ asset('assets/img/cards/card4.png') }}" class="img-fluid"
                        alt="Carte de visite">
                    <div id="carte-de-visite-texte">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Intégration de Bootstrap JS (optionnel, nécessaire pour certaines fonctionnalités) -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        function updateCarte() {
            var nom = $("#nom").val();
            var prenom = $("#prenom").val();
            var entreprise = $("#entreprise").val();

            var texteCarte = "<p>Nom: " + nom + "</p><p>Prénom: " + prenom + "</p><p>Entreprise: " + entreprise + "</p>";

            $("#carte-de-visite-texte").html(
                "<img id='image-sur-carte' src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQuU_BzP5DWSmL24gwH9VfbiY28dFG3dQ5wMA&usqp=CAU' height=90px  width=90px alt='Image sur carte'>"
            );
        }
    </script>
</body>

</html>
