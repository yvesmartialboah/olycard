<!-- signup section Here -->
<section class="signup__section pt-120 pb-120">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-lg-6">
                <div class="signup__boxes round16">
                    <h3 class="title mb-16">
                        Connexion
                    </h3>
                    <p class="fz-16 title fw-400 inter mb-40">
                        @include('flashmessage')
                    </p>
                    <form action="{{ route('users.login') }}" class="write__review" method="POST">
                        {{ csrf_field() }}
                        <div class="row g-4 ">
                            <div class="col-lg-12">
                                <div class="frm__grp">
                                    <label for="email" class="fz-18 fw-500 inter title mb-16">Adresse Email
                                    </label>
                                    <input type="text" value="{{ old('email') }}" id="email" name="email"
                                        placeholder="Entrez votre adresse email...">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="frm__grp">
                                    <label for="pas" class="fz-18 fw-500 inter title mb-16">Mot de passe
                                    </label>
                                    <input type="password" name="password" id="pas" placeholder="XXXXXX">
                                    <a href="#" class="base fz-14 inter d-flex justify-content-end mt-2">Mot de
                                        passe oublié</a>
                                </div>
                            </div>
                            <p class="fz-16 fw-400 title inter">
                                Avez-vous un compte ? <a href="{{ url('/inscription') }}" class="base">S'inscrire</a>
                            </p>
                            <div class="col-lg-6">
                                <div class="frm__grp">
                                    <button type="submit" class="cmn--btn">
                                        <span>
                                            Connexion
                                        </span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="signup__thumb">
                    <img src="{{ asset('assets/img/faq/signup-thumb.png') }}" class="w-100" alt="img">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- signup section End -->
