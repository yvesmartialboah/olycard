<!-- signup section Here -->
<section class="signup__section pt-120 pb-120">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-lg-6">
                <div class="signup__boxes round16">
                    <h3 class="title mb-16">
                        Inscription
                    </h3>
                    <p class="fz-16 title fw-400 inter mb-40">
                        @include('flashmessage')
                    </p>
                    <form action="{{ route('users.store') }}" class="write__review" method="POST">
                        {{ csrf_field() }}
                        <div class="row g-4 ">
                            <div class="col-lg-6">
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <div class="frm__grp">
                                        <label for="name" class="fz-18 fw-500 inter title mb-16">Nom <span
                                                class="text-danger">*</span>
                                        </label>
                                        <input type="text" value="{{ old('name') }}" id="name" name="name"
                                            placeholder="Entrez votre nom" required>
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong class="text-danger">{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group{{ $errors->has('prenom') ? ' has-error' : '' }}">
                                    <div class="frm__grp">
                                        <label for="prenom" class="fz-18 fw-500 inter title mb-16">Prénoms <span
                                                class="text-danger">*</span>
                                        </label>
                                        <input type="text" value="{{ old('prenom') }}" id="prenom" name="prenom"
                                            placeholder="Entrez votre prénom" required>
                                        @if ($errors->has('prenom'))
                                            <span class="help-block">
                                                <strong class="text-danger">{{ $errors->first('prenom') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <div class="frm__grp">
                                        <label for="email" class="fz-18 fw-500 inter title mb-16">Email <span
                                                class="text-danger">*</span>
                                        </label>
                                        <input type="text" value="{{ old('email') }}" id="email" name="email"
                                            placeholder="Entrez votre email" required>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong class="text-danger">{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group{{ $errors->has('contact_1') ? ' has-error' : '' }}">
                                    <div class="frm__grp">
                                        <label for="contact_1" class="fz-18 fw-500 inter title mb-16">Numéro de
                                            téléphone <span class="text-danger">*</span>
                                        </label>
                                        <input min="10" max="14" type="text"
                                            value="{{ old('contact_1') }}" id="contact_1" name="contact_1"
                                            placeholder="Entrez votre numéro de téléphone" required>
                                        @if ($errors->has('contact_1'))
                                            <span class="help-block">
                                                <strong class="text-danger">{{ $errors->first('contact_1') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-group{{ $errors->has('entreprise') ? ' has-error' : '' }}">
                                    <div class="frm__grp">
                                        <label for="entreprise" class="fz-18 fw-500 inter title mb-16">Entreprise
                                            <span class="text-danger"></span>
                                        </label>
                                        <input type="text" value="{{ old('entreprise') }}" id="entreprise"
                                            name="entreprise" password="Entrez votre prénom" required>
                                        @if ($errors->has('entreprise'))
                                            <span class="help-block">
                                                <strong class="text-danger">{{ $errors->first('entreprise') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <div class="frm__grp">
                                        <label for="password" class="fz-18 fw-500 inter title mb-16">Mot de passe
                                            <span class="text-danger">*</span>
                                        </label>
                                        <input type="password" value="{{ old('password') }}" id="password"
                                            name="password" password="Entrez votre prénom" required>
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong class="text-danger">{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                    <div class="frm__grp">
                                        <label for="password_confirmation"
                                            class="fz-18 fw-500 inter title mb-16">Confirmation
                                            <span class="text-danger">*</span>
                                        </label>
                                        <input type="password" value="{{ old('password_confirmation') }}"
                                            id="password_confirmation" name="password_confirmation"
                                            password="Entrez votre prénom" required>
                                        @if ($errors->has('password_confirmation'))
                                            <span class="help-block">
                                                <strong
                                                    class="text-danger">{{ $errors->first('password_confirmation') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>



                            <p class="fz-16 fw-400 title inter">
                                <input type="checkbox" name="" required id=""> Je lis et j'accepte
                                les <a class="base" href="">politiques de
                                    confidentialité</a>
                            </p>

                            <p class="fz-16 fw-400 title inter">
                                Avez-vous déjà un compte ? <a href="{{ url('/login-users') }}" class="base">Se
                                    connecter</a>
                            </p>
                            <div class="col-lg-6">
                                <div class="frm__grp">
                                    <button type="submit" class="cmn--btn">
                                        <span>
                                            Connexion
                                        </span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="signup__thumb">
                    <img src="{{ asset('assets/img/faq/signup-thumb.png') }}" class="w-100" alt="img">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- signup section End -->
