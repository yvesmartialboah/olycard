    <section class="search__card">
        <div class="container">
            <div class="find__searchcard">
                <div class="row g-3 justify-content-center align-items-end">
                    <div class="col-xxl-5 col-xl-5 col-lg-5 col-md-6 col-sm-6">
                        <div class="card__find__item">
                            <span class="fz-18 fw-500 inter title mb-16 d-block">
                                What's your credit score?
                            </span>
                            <select name="card1">
                                <option value="1">
                                    Excellent (740+)
                                </option>
                                <option value="1">
                                    Excellent (840+)
                                </option>
                                <option value="1">
                                    Excellent (840+)
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xxl-5 col-xl-5 col-lg-4 col-md-6 col-sm-6">
                        <div class="card__find__item">
                            <span class="fz-18 fw-500 inter title mb-16 d-block">
                                What are you looking for?
                            </span>
                            <select name="card1">
                                <option value="1">
                                    Reward
                                </option>
                                <option value="1">
                                    EReward [1]
                                </option>
                                <option value="1">
                                    Reward [2]
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xxl-2 col-xl-2 col-lg-3 col-md-4 col-sm-5">
                        <div class="card__find__item">
                            <a href="#0" class="cmn--btn">
                                <span>
                                    Find Your Card
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
