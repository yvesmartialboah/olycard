    <section class="choose__section ralt bgadd pb-120 pt-120">
        <div class="container">
            <div class="row g-4 align-items-center">
                <div class="col-xl-6 col-lg-6">
                    <div class="choose__thumb ralt">
                        <img src="{{ asset('assets/img/work/choose-card.png') }}" alt="card"
                            class="choose__mthumb w-100">
                        <img src="{{ asset('assets/img/work/map.png" alt="map-img') }}" class="mapthumb">
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6">
                    <div class="choose__content">
                        <div class="section__title mb-40">
                            <h4 class="sub ralt base mb-16 wow fadeInUp" data-wow-duration="1.1s">

                            </h4>
                            <h2 class="title mb-24 wow fadeInUp" data-wow-duration="1.2s">
                                Une synergie entre deux technologies pour créer une solution de partage innovante.
                            </h2>
                            <p class="ptext2 fz-16 fw-400 inter wow fadeInUp" data-wow-duration="1.4s">
                                Olycard, sans besoin d'application, fonctionne avec tous les smartphones. Approchez
                                simplement votre WeCard du téléphone pour échanger instantanément vos infos pro via NFC
                                ou le QR Code
                            </p>

                            <p class="ptext2 fz-16 fw-400 inter wow fadeInUp" data-wow-duration="1.4s">
                                Olycard intègre la technologie NFC (sans contact) via une puce sécurisée de haute
                                qualité, permettant une utilisation fluide et sécurisée.
                            </p>

                            <p class="ptext2 fz-16 fw-400 inter wow fadeInUp" data-wow-duration="1.4s">
                                Avec le QR Code, envoyez facilement vos données depuis votre WeCard à des utilisateurs
                                de smartphones plus anciens, simplifiant ainsi le partage d'informations.
                            </p>
                        </div>
                        {{-- <ul class="choose__listwrap">
                            <li class="d-flex mb-24 align-items-center wow fadeInUp" data-wow-duration="1.7s">
                                <i
                                    class="material-symbols-outlined base ifz32 bord round50 d-flex align-items-center justify-content-center">
                                    handshake
                                </i>
                                <span class="contentbox">
                                    <span class="fz-24 d-block mb-1 fw-600 inter title">
                                        Trustworthiness
                                    </span>
                                    <span class="ptext2 fz-16 fw-400 inter">
                                        Your creditcard marktplace has a reputation for being trustworthy
                                    </span>
                                </span>
                            </li>
                            <li class="d-flex align-items-center wow fadeInUp" data-wow-duration="1.9s">
                                <i
                                    class="material-symbols-outlined base2 ifz32 bord2 round50 d-flex align-items-center justify-content-center">
                                    arming_countdown
                                </i>
                                <span class="contentbox">
                                    <span class="fz-24 d-block mb-1 fw-600 inter title">
                                        Security & Support
                                    </span>
                                    <span class="ptext2 fz-16 fw-400 inter">
                                        Your creditcard marktplace has a reputation for being trustworthy
                                    </span>
                                </span>
                            </li>
                        </ul> --}}
                        <div class="marketplace__user pt-40 d-flex align-items-center flex-wrap wow fadeInDown">
                            {{-- <div class="d-flex janas__item gap-3 align-items-center">
                                <div class="icon">
                                    <img src="assets/img/work/janos.png" alt="img">
                                </div>
                                <div class="content">
                                    <span class="fz-18 fw-500 d-block  title mb-1">
                                        Jonas Dahito
                                    </span>
                                    <span class="fz-14 ptext">
                                        CEO & Co Founder
                                    </span>
                                </div>
                            </div> --}}
                            <a href="listing-card.html" class="cmn--btn">
                                <span>
                                    Voir +
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
