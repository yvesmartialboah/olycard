    <section class="top__reted pb-120 pt-120">
        <div class="container">
            <div class="row align-items-center mb-20 g-3 justify-content-between">
                <div class="col-lg-5">
                    <div class="section__title">
                        <h4 class="sub ralt base mb-16 wow fadeInUp">
                            Categories
                        </h4>
                        <h2 class="title wow fadeInUp">
                            Explore Our Top-Rated Categories
                        </h2>
                    </div>
                </div>
                <div class="col-lg-3">
                    <a href="gift-card.html" class="cmn--btn wow fadeInDown">
                        <span>
                            See All Categories
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="top__ratedwrapper owl-theme owl-carousel">
            <div class="top__items suctom__space bgwhite round16 bgwhite">
                <div class="icon m-auto d-flex align-items-center justify-content-center boxes2 round50">
                    <i class="material-symbols-outlined icolor1">
                        account_balance_wallet
                    </i>
                </div>
                <div class="content text-center mt-24">
                    <h3 class="mb-16">
                        <a href="gift-card.html" class="title">
                            Balance transfer
                        </a>
                    </h3>
                    <p class="fz-16 fw-400 inter ptext2 mb-30">
                        These credit cards offer rewards for spending, such as cashback
                    </p>
                    <a href="gift-card.html"
                        class="arrow m-auto boxes4 round50 d-flex align-items-center justify-content-center">
                        <i class="material-symbols-outlined">
                            chevron_right
                        </i>
                    </a>
                </div>
            </div>
            <div class="top__items suctom__space bgwhite round16 bgwhite">
                <div class="icon m-auto d-flex align-items-center icolor2 justify-content-center boxes3 round50">
                    <i class="material-symbols-outlined">
                        credit_card
                    </i>
                </div>
                <div class="content text-center mt-24">
                    <h3 class="mb-16">
                        <a href="gift-card.html" class="title">
                            Student cards
                        </a>
                    </h3>
                    <p class="fz-16 fw-400 inter ptext2 mb-30">
                        These credit cards offer rewards for spending, such as cashback
                    </p>
                    <a href="gift-card.html"
                        class="arrow m-auto boxes4 round50 d-flex align-items-center justify-content-center">
                        <i class="material-symbols-outlined">
                            chevron_right
                        </i>
                    </a>
                </div>
            </div>
            <div class="top__items suctom__space bgwhite round16 bgwhite">
                <div class="icon m-auto d-flex align-items-center base justify-content-center boxes4 round50">
                    <i class="material-symbols-outlined">
                        social_leaderboard
                    </i>
                </div>
                <div class="content text-center mt-24">
                    <h3 class="mb-16">
                        <a href="gift-card.html" class="title">
                            Rewards cards
                        </a>
                    </h3>
                    <p class="fz-16 fw-400 inter ptext2 mb-30">
                        These credit cards offer rewards for spending, such as cashback
                    </p>
                    <a href="gift-card.html"
                        class="arrow m-auto boxes4 round50 d-flex align-items-center justify-content-center">
                        <i class="material-symbols-outlined">
                            chevron_right
                        </i>
                    </a>
                </div>
            </div>
            <div class="top__items suctom__space bgwhite round16 bgwhite">
                <div class="icon m-auto d-flex align-items-center icolor1 justify-content-center boxes2 round50">
                    <i class="material-symbols-outlined">
                        monitoring
                    </i>
                </div>
                <div class="content text-center mt-24">
                    <h3 class="mb-16">
                        <a href="gift-card.html" class="title">
                            Business cards
                        </a>
                    </h3>
                    <p class="fz-16 fw-400 inter ptext2 mb-30">
                        These credit cards offer rewards for spending, such as cashback
                    </p>
                    <a href="gift-card.html"
                        class="arrow m-auto boxes4 round50 d-flex align-items-center justify-content-center">
                        <i class="material-symbols-outlined">
                            chevron_right
                        </i>
                    </a>
                </div>
            </div>
            <div class="top__items suctom__space bgwhite round16 bgwhite">
                <div class="icon m-auto d-flex align-items-center justify-content-center boxes2 round50">
                    <i class="material-symbols-outlined icolor1">
                        account_balance_wallet
                    </i>
                </div>
                <div class="content text-center mt-24">
                    <h3 class="mb-16">
                        <a href="gift-card.html" class="title">
                            Balance transfer
                        </a>
                    </h3>
                    <p class="fz-16 fw-400 inter ptext2 mb-30">
                        These credit cards offer rewards for spending, such as cashback
                    </p>
                    <a href="gift-card.html"
                        class="arrow m-auto boxes4 round50 d-flex align-items-center justify-content-center">
                        <i class="material-symbols-outlined">
                            chevron_right
                        </i>
                    </a>
                </div>
            </div>
            <div class="top__items suctom__space bgwhite round16 bgwhite">
                <div class="icon m-auto d-flex align-items-center icolor2 justify-content-center boxes3 round50">
                    <i class="material-symbols-outlined">
                        credit_card
                    </i>
                </div>
                <div class="content text-center mt-24">
                    <h3 class="mb-16">
                        <a href="gift-card.html" class="title">
                            Student cards
                        </a>
                    </h3>
                    <p class="fz-16 fw-400 inter ptext2 mb-30">
                        These credit cards offer rewards for spending, such as cashback
                    </p>
                    <a href="gift-card.html"
                        class="arrow m-auto boxes4 round50 d-flex align-items-center justify-content-center">
                        <i class="material-symbols-outlined">
                            chevron_right
                        </i>
                    </a>
                </div>
            </div>
            <div class="top__items suctom__space bgwhite round16 bgwhite">
                <div class="icon m-auto d-flex align-items-center base justify-content-center boxes4 round50">
                    <i class="material-symbols-outlined">
                        social_leaderboard
                    </i>
                </div>
                <div class="content text-center mt-24">
                    <h3 class="mb-16">
                        <a href="gift-card.html" class="title">
                            Rewards cards
                        </a>
                    </h3>
                    <p class="fz-16 fw-400 inter ptext2 mb-30">
                        These credit cards offer rewards for spending, such as cashback
                    </p>
                    <a href="gift-card.html"
                        class="arrow m-auto boxes4 round50 d-flex align-items-center justify-content-center">
                        <i class="material-symbols-outlined">
                            chevron_right
                        </i>
                    </a>
                </div>
            </div>
            <div class="top__items suctom__space bgwhite round16 bgwhite">
                <div class="icon m-auto d-flex align-items-center icolor1 justify-content-center boxes2 round50">
                    <i class="material-symbols-outlined">
                        monitoring
                    </i>
                </div>
                <div class="content text-center mt-24">
                    <h3 class="mb-16">
                        <a href="gift-card.html" class="title">
                            Business cards
                        </a>
                    </h3>
                    <p class="fz-16 fw-400 inter ptext2 mb-30">
                        These credit cards offer rewards for spending, such as cashback
                    </p>
                    <a href="gift-card.html"
                        class="arrow m-auto boxes4 round50 d-flex align-items-center justify-content-center">
                        <i class="material-symbols-outlined">
                            chevron_right
                        </i>
                    </a>
                </div>
            </div>
        </div>
    </section>
