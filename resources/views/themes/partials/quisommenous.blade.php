    <section class="provide__section ralt bgadd pb-120 pt-120">
        <div class="container">
            <div class="row g-4 align-items-center justify-content-between">
                <div class="col-xl-6 col-lg-7">
                    <div class="choose__content">
                        <div class="section__title mb-40">
                            <h4 class="sub ralt base mb-16 wow fadeInUp">
                                Who We Are
                            </h4>
                            <h2 class="title mb-24 wow fadeInDown">
                                We Provide Trusted Credit Card For This World
                            </h2>
                            <p class="ptext2 fz-16 fw-400 inter wow fadeInUp">
                                We understand that the world of credit cards can be overwhelming, with countless options
                                available and complex terms and conditions. That's why we're here to guide you through
                                the process and help you make informed decisions.
                            </p>
                        </div>
                        <ul class="choose__listwrap provide__listwrap d-flex align-items-center">
                            <li class="d-flex mb-24 wow fadeInDown">
                                <i
                                    class="material-symbols-outlined base ifz32 bgwhite round50 d-flex align-items-center justify-content-center">
                                    savings
                                </i>
                                <span class="contentbox">
                                    <span class="fz-24 d-block mb-1 fw-600 inter title">
                                        Money Saving
                                    </span>
                                    <span class="ptext2 fz-16 fw-400 inter">
                                        We understand that the world of credit cards
                                    </span>
                                </span>
                            </li>
                            <li class="d-flex wow fadeInUp">
                                <i
                                    class="material-symbols-outlined icolor1 ifz32 bgwhite round50 d-flex align-items-center justify-content-center">
                                    alarm_on
                                </i>
                                <span class="contentbox">
                                    <span class="fz-24 d-block mb-1 fw-600 inter title">
                                        Time Saving
                                    </span>
                                    <span class="ptext2 fz-16 fw-400 inter">
                                        We understand that the world of credit cards
                                    </span>
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-5 col-lg-5">
                    <div class="provide__thumb">
                        <img src="assets/img/work/provide.png" alt="card" class="w-100">
                    </div>
                </div>
            </div>
            <div class="counter__wrap mt-100 px-4 pt-120 pb-120 round16">
                <div class="row g-3 justify-content-center">
                    <div class="col-xxl-3 col-xl-3 col-lg-6 col-md-6 col-sm-8">
                        <div class="counter__items odometer-item wow fadeInDown">
                            <div class="counter__content justify-content-center d-flex align-items-center">
                                <div class="iconbox d-flex align-items-center justify-content-center round50 boxes1">
                                    <i class="material-symbols-outlined base">
                                        add_card
                                    </i>
                                </div>
                                <div class="content">
                                    <div class="d-flex mb-1 align-items-center">
                                        <span class="odometer base" data-odometer-final="2.5">
                                            0
                                        </span>
                                        <span class="added base">
                                            k
                                        </span>
                                    </div>
                                    <span class="fz-16 fw-400 ptext2 inter">
                                        Total Card
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xxl-3 col-xl-3 col-lg-6 col-md-6 col-sm-8">
                        <div class="counter__items odometer-item wow fadeInUp">
                            <div class="counter__content justify-content-center d-flex align-items-center">
                                <div class="iconbox d-flex align-items-center justify-content-center round50 boxes2">
                                    <i class="material-symbols-outlined icolor1">
                                        monitoring
                                    </i>
                                </div>
                                <div class="content">
                                    <div class="d-flex mb-1 align-items-center">
                                        <span class="odometer icolor1" data-odometer-final="3.8">
                                            0
                                        </span>
                                        <span class="added icolor1">
                                            k
                                        </span>
                                    </div>
                                    <span class="fz-16 fw-400 ptext2 inter">
                                        Total Sale
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xxl-3 col-xl-3 col-lg-6 col-md-6 col-sm-8">
                        <div class="counter__items odometer-item wow fadeInDown">
                            <div class="counter__content justify-content-center d-flex align-items-center">
                                <div class="iconbox d-flex align-items-center justify-content-center round50 boxes3">
                                    <i class="material-symbols-outlined icolor2">
                                        handshake
                                    </i>
                                </div>
                                <div class="content">
                                    <div class="d-flex mb-1 align-items-center">
                                        <span class="odometer icolor2" data-odometer-final="7.5">
                                            0
                                        </span>
                                        <span class="added icolor2">
                                            k
                                        </span>
                                    </div>
                                    <span class="fz-16 fw-400 ptext2 inter">
                                        Client Satisfied
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xxl-3 col-xl-3 col-lg-6 col-md-6 col-sm-8">
                        <div class="counter__items odometer-item wow fadeInUp">
                            <div class="counter__content justify-content-center d-flex align-items-center">
                                <div class="iconbox d-flex align-items-center justify-content-center round50 boxes1">
                                    <i class="material-symbols-outlined base">
                                        social_leaderboard
                                    </i>
                                </div>
                                <div class="content">
                                    <div class="d-flex mb-1 align-items-center">
                                        <span class="odometer base" data-odometer-final="60">
                                            0
                                        </span>
                                        <span class="added base">
                                            +
                                        </span>
                                    </div>
                                    <span class="fz-16 fw-400 ptext2 inter">
                                        Total Award
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ball">
            <img src="assets/img/banner/ball.png" alt="ball">
        </div>
    </section>
