    <section class="banner__section bgadd overhid ralt">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-xl-6 col-lg-6">
                    <div class="banner__content ralt">
                        {{-- <h4 class="sub ralt base mb-16 wow fadeInDown" data-wow-duration="0.4s">
                            Our Top Picks
                        </h4> --}}
                        <span class="d3 mb-24 title wow fadeInDown" data-wow-duration="0.6s">
                            Votre carte de visite <br> <span class="gratext">Numérique.</span>
                        </span>
                        <p class="mb-40 title wow fadeInDown" data-wow-duration="0.8s">
                            Professionnel, développez votre réseau numérique
                        </p>
                        <div class="btn__grp d-flex align-items-center wow fadeInDown" data-wow-duration="1s">
                            <a href="listing-card.html" class="cmn--btn">
                                <span>
                                    Découvrir nos offres
                                </span>

                            </a>
                            {{-- <a href="signup.html" class="cmn--btn outline__btn">
                                <span>
                                    Register Now
                                </span>
                            </a> --}}
                        </div>
                        <div class="ball">
                            <img src="assets/img/banner/ball.png" alt="img">
                        </div>
                    </div>
                </div>
                <div class="col-xl-5 col-lg-5">
                    <div class="banner__shapethumb ralt">
                        <div class="thumb">
                            <img src="assets/img/banner/banner-thumb1.png" class="w-100" alt="banner">
                        </div>
                        <div class="circle__thumb">
                            <img src="assets/img/banner/banner-thumbbg1.png" alt="shape">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wallet">
            <img src="assets/img/banner/wallet.png" alt="img">
        </div>
        <div class="hand-wallet">
            <img src="assets/img/banner/hand-wallet.png" alt="img">
        </div>
        <div class="mobile-wallet">
            <img src="assets/img/banner/mobile-wallet.png" alt="img">
        </div>
    </section>
