    <section class="app__section ralt bgadd pb-120 pt-120">
        <div class="container">
            <div class="row g-4 align-items-center justify-content-between">
                <div class="col-xl-5 col-lg-5">
                    <div class="app__thumb ralt">
                        <img src="assets/img/app/app1.png" alt="card" class="w-100">
                    </div>
                </div>
                <div class="col-xl-6 col-lg-7">
                    <div class="app__content">
                        <div class="section__title mb-30">
                            <h4 class="sub ralt base mb-16 wow fadeInDown">
                                Download Our Apps
                            </h4>
                            <h2 class="title mb-24 wow fadeInUp">
                                Follow Four Quick Steps To Buy Our Card With Confidence
                            </h2>
                            <p class="ptext2 fz-16 fw-400 inter wow fadeInDown">
                                Take your credit card search on the go with our easy-to-use mobile apps. Download them
                                today and get access to our powerful credit card search tools anytime, anywhere. With
                                our apps, you can
                            </p>
                        </div>
                        <ul class="app__listwrap mb-40">
                            <li>
                                <a href="#0" class="d-flex align-items-center wow fadeInUp">
                                    <i
                                        class="material-symbols-outlined round50 d-flex align-items-center justify-content-center text-white">
                                        south
                                    </i>
                                    <span class="fz-20 fw-500 inter title">
                                        Download App
                                    </span>
                                </a>
                            </li>
                            <li class="d-flex align-items-center">
                                <a href="#0" class="d-flex align-items-center wow fadeInUp">
                                    <i
                                        class="material-symbols-outlined round50 d-flex align-items-center justify-content-center text-white">
                                        south
                                    </i>
                                    <span class="fz-20 fw-500 inter title">
                                        Search and Compare
                                    </span>
                                </a>
                            </li>
                            <li class="d-flex align-items-center">
                                <a href="#0" class="d-flex align-items-center wow fadeInUp">
                                    <i
                                        class="material-symbols-outlined round50 d-flex align-items-center justify-content-center text-white">
                                        south
                                    </i>
                                    <span class="fz-20 fw-500 inter title">
                                        Select a Card
                                    </span>
                                </a>
                            </li>
                            <li class="d-flex align-items-center">
                                <a href="#0" class="d-flex align-items-center wow fadeInUp">
                                    <i
                                        class="material-symbols-outlined round50 d-flex align-items-center justify-content-center text-white">
                                        south
                                    </i>
                                    <span class="fz-20 fw-500 inter title">
                                        Apply for the Card
                                    </span>
                                </a>
                            </li>
                        </ul>
                        <div class="app__store d-flex align-items-center gap-3 flex-wrap wow fadeInDown">
                            <a href="javascript:void(0)">
                                <img src="assets/img/app/appstore.png" alt="app">
                            </a>
                            <a href="javascript:void(0)">
                                <img src="assets/img/app/googlepaly.png" alt="app">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="shape__ball">
            <img src="assets/img/banner/ball.png" alt="ball">
        </div>
    </section>
