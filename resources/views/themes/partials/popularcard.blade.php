    <section class="popular__card bgadd ralt pt-120 pb-120">
        <div class="container">
            <div class="row justify-content-center mb-40">
                <div class="col-lg-6 col-md-9">
                    <div class="section__title text-center">
                        <h4 class="sub ralt base mb-16 wow fadeInDown">

                        </h4>
                        <h2 class="title wow fadeInUp">
                            Rejoignez la communautée
                        </h2>
                        {{-- <p class="ptext2 fz-16 fw-400 inter wow fadeInDown">
                            Looking for a credit card that's trusted and highly rated? Look no further than our Most
                            Popular Cards section
                        </p> --}}
                    </div>
                </div>
            </div>
            <div class="popular__tabs">
                <div class="nav nav-tabs mb-60" id="nav-tab" role="tablist">
                    {{-- <button class="nav-link active" id="nav-home-tab" data-bs-toggle="tab" data-bs-target="#nav-home"
                        type="button" role="tab" aria-controls="nav-home" aria-selected="true">
                        <span class="icon">
                            <i class="material-symbols-outlined">
                                new_releases
                            </i>
                        </span>
                        <span>
                            Best of 2023
                        </span>
                    </button>
                    <button class="nav-link" id="nav-profile-tab" data-bs-toggle="tab" data-bs-target="#nav-profile"
                        type="button" role="tab" aria-controls="nav-profile" aria-selected="false">
                        <span class="icon">
                            <i class="material-symbols-outlined">
                                bar_chart_4_bars
                            </i>
                        </span>
                        <span>
                            0% ARP
                        </span>
                    </button>
                    <button class="nav-link" id="nav-contact-tab" data-bs-toggle="tab" data-bs-target="#nav-contact"
                        type="button" role="tab" aria-controls="nav-contact" aria-selected="false">
                        <span class="icon">
                            <i class="material-symbols-outlined">
                                local_atm
                            </i>
                        </span>
                        <span>
                            Cash Back
                        </span>
                    </button>
                    <button class="nav-link" id="nav-contact-tab1" data-bs-toggle="tab" data-bs-target="#nav-contact1"
                        type="button" role="tab" aria-controls="nav-contact1" aria-selected="false">
                        <span class="icon">
                            <i class="material-symbols-outlined">
                                bluetooth_drive
                            </i>
                        </span>
                        <span>
                            Travels
                        </span>
                    </button> --}}
                </div>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
                        aria-labelledby="nav-home-tab">
                        <div class="row g-4">
                            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6">
                                <div class="popular__items round16">
                                    <img src="assets/img/cards/card4.png" alt="card" class="w-100">
                                    <div class="content text-center">
                                        <div
                                            class="ratting mb-15 justify-content-center d-flex align-items-center gap-2">
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                        </div>
                                        <h4 class="title mb-16">

                                            Olycard Standard
                                        </h4>
                                        <h4 class="title mb-16 text-danger">
                                            15 000F
                                        </h4>
                                        {{-- <p class="fz-16 mb-30 inter ptext">
                                            We understand that the world of credit cards can be overwhelming
                                        </p> --}}
                                        <a href="card-details.html"
                                            class="cmn--btn gap-2 outline__btn d-flex align-items-center ">
                                            <span>
                                                Voir +
                                            </span>
                                            <span class="icon">
                                                <i class="material-symbols-outlined">
                                                    arrow_right_alt
                                                </i>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6">
                                <div class="popular__items round16">
                                    <img src="assets/img/cards/card5.png" alt="card" class="w-100">
                                    <div class="content text-center">
                                        <div
                                            class="ratting mb-15 justify-content-center d-flex align-items-center gap-2">
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                        </div>
                                        <h4 class="title mb-16">
                                            Olycard Business
                                        </h4>
                                        <h4 class="title mb-16 text-danger">
                                            30 000F
                                        </h4>
                                        <a href="card-details.html"
                                            class="cmn--btn gap-2 outline__btn d-flex align-items-center ">
                                            <span>
                                                Voir +
                                            </span>
                                            <span class="icon">
                                                <i class="material-symbols-outlined">
                                                    arrow_right_alt
                                                </i>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6">
                                <div class="popular__items round16">
                                    <img src="assets/img/cards/card6.png" alt="card" class="w-100">
                                    <div class="content text-center">
                                        <div
                                            class="ratting mb-15 justify-content-center d-flex align-items-center gap-2">
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                        </div>
                                        <h4 class="title mb-16">
                                            Olycard Luxe
                                        </h4>
                                        <h4 class="title mb-16 text-danger">
                                            50 000F
                                        </h4>
                                        <a href="card-details.html"
                                            class="cmn--btn gap-2 outline__btn d-flex align-items-center ">
                                            <span>
                                                View Details
                                            </span>
                                            <span class="icon">
                                                <i class="material-symbols-outlined">
                                                    arrow_right_alt
                                                </i>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <div class="row g-4">
                            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6">
                                <div class="popular__items round16">
                                    <img src="assets/img/cards/card1.png" alt="card" class="w-100">
                                    <div class="content text-center">
                                        <div
                                            class="ratting mb-15 justify-content-center d-flex align-items-center gap-2">
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                        </div>
                                        <h4 class="title mb-16">
                                            JK Bank Ltd
                                        </h4>
                                        <p class="fz-16 mb-30 inter ptext">
                                            We understand that the world of credit cards can be overwhelming
                                        </p>
                                        <a href="card-details.html"
                                            class="cmn--btn gap-2 outline__btn d-flex align-items-center ">
                                            <span>
                                                Voir +
                                            </span>
                                            <span class="icon">
                                                <i class="material-symbols-outlined">
                                                    arrow_right_alt
                                                </i>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6">
                                <div class="popular__items round16">
                                    <img src="assets/img/cards/card2.png" alt="card" class="w-100">
                                    <div class="content text-center">
                                        <div
                                            class="ratting mb-15 justify-content-center d-flex align-items-center gap-2">
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                        </div>
                                        <h4 class="title mb-16">
                                            CT Bank Ltd
                                        </h4>
                                        <p class="fz-16 mb-30 inter ptext">
                                            We understand that the world of credit cards can be overwhelming
                                        </p>
                                        <a href="card-details.html"
                                            class="cmn--btn gap-2 outline__btn d-flex align-items-center ">
                                            <span>
                                                View Details
                                            </span>
                                            <span class="icon">
                                                <i class="material-symbols-outlined">
                                                    arrow_right_alt
                                                </i>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6">
                                <div class="popular__items round16">
                                    <img src="assets/img/cards/card3.png" alt="card" class="w-100">
                                    <div class="content text-center">
                                        <div
                                            class="ratting mb-15 justify-content-center d-flex align-items-center gap-2">
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                        </div>
                                        <h4 class="title mb-16">
                                            AK Bank Ltd
                                        </h4>
                                        <p class="fz-16 mb-30 inter ptext">
                                            We understand that the world of credit cards can be overwhelming
                                        </p>
                                        <a href="card-details.html"
                                            class="cmn--btn gap-2 outline__btn d-flex align-items-center ">
                                            <span>
                                                View Details
                                            </span>
                                            <span class="icon">
                                                <i class="material-symbols-outlined">
                                                    arrow_right_alt
                                                </i>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6">
                                <div class="popular__items round16">
                                    <img src="assets/img/cards/card4.png" alt="card" class="w-100">
                                    <div class="content text-center">
                                        <div
                                            class="ratting mb-15 justify-content-center d-flex align-items-center gap-2">
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                        </div>
                                        <h4 class="title mb-16">
                                            Bank of America
                                        </h4>
                                        <p class="fz-16 mb-30 inter ptext">
                                            We understand that the world of credit cards can be overwhelming
                                        </p>
                                        <a href="card-details.html"
                                            class="cmn--btn gap-2 outline__btn d-flex align-items-center ">
                                            <span>
                                                View Details
                                            </span>
                                            <span class="icon">
                                                <i class="material-symbols-outlined">
                                                    arrow_right_alt
                                                </i>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6">
                                <div class="popular__items round16">
                                    <img src="assets/img/cards/card5.png" alt="card" class="w-100">
                                    <div class="content text-center">
                                        <div
                                            class="ratting mb-15 justify-content-center d-flex align-items-center gap-2">
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                        </div>
                                        <h4 class="title mb-16">
                                            TD Bank Ltd
                                        </h4>
                                        <p class="fz-16 mb-30 inter ptext">
                                            We understand that the world of credit cards can be overwhelming
                                        </p>
                                        <a href="card-details.html"
                                            class="cmn--btn gap-2 outline__btn d-flex align-items-center ">
                                            <span>
                                                View Details
                                            </span>
                                            <span class="icon">
                                                <i class="material-symbols-outlined">
                                                    arrow_right_alt
                                                </i>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6">
                                <div class="popular__items round16">
                                    <img src="assets/img/cards/card6.png" alt="card" class="w-100">
                                    <div class="content text-center">
                                        <div
                                            class="ratting mb-15 justify-content-center d-flex align-items-center gap-2">
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                        </div>
                                        <h4 class="title mb-16">
                                            Wells Fargo Ltd
                                        </h4>
                                        <p class="fz-16 mb-30 inter ptext">
                                            We understand that the world of credit cards can be overwhelming
                                        </p>
                                        <a href="card-details.html"
                                            class="cmn--btn gap-2 outline__btn d-flex align-items-center ">
                                            <span>
                                                View Details
                                            </span>
                                            <span class="icon">
                                                <i class="material-symbols-outlined">
                                                    arrow_right_alt
                                                </i>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                        <div class="row g-4">
                            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6">
                                <div class="popular__items round16">
                                    <img src="assets/img/cards/card1.png" alt="card" class="w-100">
                                    <div class="content text-center">
                                        <div
                                            class="ratting mb-15 justify-content-center d-flex align-items-center gap-2">
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                        </div>
                                        <h4 class="title mb-16">
                                            JK Bank Ltd
                                        </h4>
                                        <p class="fz-16 mb-30 inter ptext">
                                            We understand that the world of credit cards can be overwhelming
                                        </p>
                                        <a href="card-details.html"
                                            class="cmn--btn gap-2 outline__btn d-flex align-items-center ">
                                            <span>
                                                View Details
                                            </span>
                                            <span class="icon">
                                                <i class="material-symbols-outlined">
                                                    arrow_right_alt
                                                </i>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6">
                                <div class="popular__items round16">
                                    <img src="assets/img/cards/card2.png" alt="card" class="w-100">
                                    <div class="content text-center">
                                        <div
                                            class="ratting mb-15 justify-content-center d-flex align-items-center gap-2">
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                        </div>
                                        <h4 class="title mb-16">
                                            CT Bank Ltd
                                        </h4>
                                        <p class="fz-16 mb-30 inter ptext">
                                            We understand that the world of credit cards can be overwhelming
                                        </p>
                                        <a href="card-details.html"
                                            class="cmn--btn gap-2 outline__btn d-flex align-items-center ">
                                            <span>
                                                View Details
                                            </span>
                                            <span class="icon">
                                                <i class="material-symbols-outlined">
                                                    arrow_right_alt
                                                </i>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6">
                                <div class="popular__items round16">
                                    <img src="assets/img/cards/card3.png" alt="card" class="w-100">
                                    <div class="content text-center">
                                        <div
                                            class="ratting mb-15 justify-content-center d-flex align-items-center gap-2">
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                        </div>
                                        <h4 class="title mb-16">
                                            AK Bank Ltd
                                        </h4>
                                        <p class="fz-16 mb-30 inter ptext">
                                            We understand that the world of credit cards can be overwhelming
                                        </p>
                                        <a href="card-details.html"
                                            class="cmn--btn gap-2 outline__btn d-flex align-items-center ">
                                            <span>
                                                View Details
                                            </span>
                                            <span class="icon">
                                                <i class="material-symbols-outlined">
                                                    arrow_right_alt
                                                </i>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6">
                                <div class="popular__items round16">
                                    <img src="assets/img/cards/card4.png" alt="card" class="w-100">
                                    <div class="content text-center">
                                        <div
                                            class="ratting mb-15 justify-content-center d-flex align-items-center gap-2">
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                        </div>
                                        <h4 class="title mb-16">
                                            Bank of America
                                        </h4>
                                        <p class="fz-16 mb-30 inter ptext">
                                            We understand that the world of credit cards can be overwhelming
                                        </p>
                                        <a href="card-details.html"
                                            class="cmn--btn gap-2 outline__btn d-flex align-items-center ">
                                            <span>
                                                View Details
                                            </span>
                                            <span class="icon">
                                                <i class="material-symbols-outlined">
                                                    arrow_right_alt
                                                </i>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6">
                                <div class="popular__items round16">
                                    <img src="assets/img/cards/card5.png" alt="card" class="w-100">
                                    <div class="content text-center">
                                        <div
                                            class="ratting mb-15 justify-content-center d-flex align-items-center gap-2">
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                        </div>
                                        <h4 class="title mb-16">
                                            TD Bank Ltd
                                        </h4>
                                        <p class="fz-16 mb-30 inter ptext">
                                            We understand that the world of credit cards can be overwhelming
                                        </p>
                                        <a href="card-details.html"
                                            class="cmn--btn gap-2 outline__btn d-flex align-items-center ">
                                            <span>
                                                View Details
                                            </span>
                                            <span class="icon">
                                                <i class="material-symbols-outlined">
                                                    arrow_right_alt
                                                </i>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6">
                                <div class="popular__items round16">
                                    <img src="assets/img/cards/card6.png" alt="card" class="w-100">
                                    <div class="content text-center">
                                        <div
                                            class="ratting mb-15 justify-content-center d-flex align-items-center gap-2">
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                        </div>
                                        <h4 class="title mb-16">
                                            Wells Fargo Ltd
                                        </h4>
                                        <p class="fz-16 mb-30 inter ptext">
                                            We understand that the world of credit cards can be overwhelming
                                        </p>
                                        <a href="card-details.html"
                                            class="cmn--btn gap-2 outline__btn d-flex align-items-center ">
                                            <span>
                                                View Details
                                            </span>
                                            <span class="icon">
                                                <i class="material-symbols-outlined">
                                                    arrow_right_alt
                                                </i>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="nav-contact1" role="tabpanel" aria-labelledby="nav-contact-tab1">
                        <div class="row g-4">
                            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6">
                                <div class="popular__items round16">
                                    <img src="assets/img/cards/card4.png" alt="card" class="w-100">
                                    <div class="content text-center">
                                        <div
                                            class="ratting mb-15 justify-content-center d-flex align-items-center gap-2">
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                        </div>
                                        <h4 class="title mb-16">
                                            Bank of America
                                        </h4>
                                        <p class="fz-16 mb-30 inter ptext">
                                            We understand that the world of credit cards can be overwhelming
                                        </p>
                                        <a href="card-details.html"
                                            class="cmn--btn gap-2 outline__btn d-flex align-items-center ">
                                            <span>
                                                View Details
                                            </span>
                                            <span class="icon">
                                                <i class="material-symbols-outlined">
                                                    arrow_right_alt
                                                </i>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6">
                                <div class="popular__items round16">
                                    <img src="assets/img/cards/card5.png" alt="card" class="w-100">
                                    <div class="content text-center">
                                        <div
                                            class="ratting mb-15 justify-content-center d-flex align-items-center gap-2">
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                        </div>
                                        <h4 class="title mb-16">
                                            TD Bank Ltd
                                        </h4>
                                        <p class="fz-16 mb-30 inter ptext">
                                            We understand that the world of credit cards can be overwhelming
                                        </p>
                                        <a href="card-details.html"
                                            class="cmn--btn gap-2 outline__btn d-flex align-items-center ">
                                            <span>
                                                View Details
                                            </span>
                                            <span class="icon">
                                                <i class="material-symbols-outlined">
                                                    arrow_right_alt
                                                </i>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6">
                                <div class="popular__items round16">
                                    <img src="assets/img/cards/card6.png" alt="card" class="w-100">
                                    <div class="content text-center">
                                        <div
                                            class="ratting mb-15 justify-content-center d-flex align-items-center gap-2">
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                            <i class="material-symbols-outlined ratting">
                                                star
                                            </i>
                                        </div>
                                        <h4 class="title mb-16">
                                            Wells Fargo Ltd
                                        </h4>
                                        <p class="fz-16 mb-30 inter ptext">
                                            We understand that the world of credit cards can be overwhelming
                                        </p>
                                        <a href="card-details.html"
                                            class="cmn--btn gap-2 outline__btn d-flex align-items-center ">
                                            <span>
                                                View Details
                                            </span>
                                            <span class="icon">
                                                <i class="material-symbols-outlined">
                                                    arrow_right_alt
                                                </i>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
