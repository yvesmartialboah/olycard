<section class="how__worksection pt-110 pb-120">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="section__title text-center mb-60">
                    <h4 class="sub ralt base mb-16 wow fadeInUp" data-wow-duration="0.5s">

                    </h4>
                    <h2 class="title mb-24 wow fadeInUp" data-wow-duration="0.7s">
                        Comment ça marche ?
                    </h2>
                    <p class="ptext2 fz-16 fw-400 inter wow fadeInUp" data-wow-duration="0.9s">
                        Les cartes de visite numériques visent à faciliter et à moderniser le processus de partage
                        d'informations professionnelles, tout en réduisant l'utilisation de supports physiques comme les
                        cartes de visite papier.
                    </p>
                    <div class="container">
                        <div class="container-fluid text-center">
                            <div class="bt__one mb-20 mt-20">
                                <video controls autoplay src="{{ asset('assets/videos/intro.mp4') }}" alt="balance"
                                    class="round16 w-100"></video>
                                {{-- <video controls autoplay>
                        <source class="round16 w-90" src="assets/videos/intro.mp4" type="video/mp4">
                        Your browser does not support the video tag.
                    </video> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row g-4 justify-content-center">
            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 wow fadeInDown" data-wow-duration="0.5s">
                <div class="howwork__item transition text-center">
                    <div class="thumb transition tershape1 m-auto round50 shadow1">
                        <img src="assets/img/work/wor1.jpg" alt="work">
                    </div>
                    <div class="content mt-24">
                        <h4 class="title mb-16">
                            <a href="#0">
                                Search and Compare
                            </a>
                        </h4>
                        <p class="fz-14 fw-400 inter title">
                            First, you'll need to search and compare credit cards offered on the credit card
                            marketplace website
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 custom-120 wow fadeInDown" data-wow-duration="0.9s">
                <div class="howwork__item transition text-center">
                    <div class="thumb transition tershape2 m-auto round50 shadow1">
                        <img src="assets/img/work/work2.jpg" alt="work">
                    </div>
                    <div class="content mt-24">
                        <h4 class="title mb-16">
                            <a href="#0">
                                Select a Card
                            </a>
                        </h4>
                        <p class="fz-14 fw-400 inter title">
                            Once you've compared the different options, select a card that meets your needs and
                            click on it for more information
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 wow fadeInDown" data-wow-duration="0.9s">
                <div class="howwork__item transition text-center">
                    <div class="thumb transition m-auto round50 shadow1">
                        <img src="assets/img/work/work3.jpg" alt="work">
                    </div>
                    <div class="content mt-24">
                        <h4 class="title mb-16">
                            <a href="#0">
                                Apply for the Card
                            </a>
                        </h4>
                        <p class="fz-14 fw-400 inter title">
                            If you decide to proceed with the card, you'll be directed to the issuer's website or
                            application form to complete
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row g-4 pt-60 justify-content-center">
            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 wow fadeInUp" data-wow-duration="1s">
                <div class="select__cardbox boxes1 d-flex align-items-center">
                    <div class="thumb transition d-flex align-items-center justify-content-center">
                        <img src="assets/img/work/account.png" alt="img">
                    </div>
                    <h4 class="title">
                        Apply For Personal Account
                    </h4>
                </div>
            </div>
            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 wow fadeInUp" data-wow-duration="1.2s">
                <div class="select__cardbox boxes2 d-flex align-items-center">
                    <div class="thumb transition d-flex align-items-center justify-content-center">
                        <img src="assets/img/work/monitizeation.png" alt="img">
                    </div>
                    <h4 class="title">
                        Apply For Business Account
                    </h4>
                </div>
            </div>
            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 wow fadeInUp" data-wow-duration="1.5s">
                <div class="select__cardbox boxes3 d-flex align-items-center">
                    <div class="thumb transition d-flex align-items-center justify-content-center">
                        <img src="assets/img/work/search.png" alt="img">
                    </div>
                    <h4 class="title">
                        Apply For Worldwide Account
                    </h4>
                </div>
            </div>
        </div>
    </div>
</section>
