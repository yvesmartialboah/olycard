    <section class="testimonial__section pb-120 pt-120">
        <div class="container">
            <div class="row g-4 align-items-center justify-content-between">
                <div class="col-xl-5 col-lg-5 col-md-8">
                    <div class="testimonial__thumb ralt wow fadeInDown">
                        <img src="assets/img/testimonial/testimonial1.png" alt="card" class="w-100">
                    </div>
                </div>
                <div class="col-xl-6 col-lg-7">
                    <div class="testimonial__content">
                        <div class="section__title mb-40">
                            <h4 class="sub ralt base mb-16 wow fadeInDown">
                                Testimonial
                            </h4>
                            <h2 class="title mb-24 wow fadeInUp">
                                Feedback From Our Valued Customers For Marketplace
                            </h2>
                            <p class="ptext2 fz-16 fw-400 inter wow fadeInDown">
                                We understand that choosing a credit card can be overwhelming, especially with so many
                                options available. That's why we've created a user-friendly platform
                            </p>
                        </div>
                        <div class="testimonial__slider owl-theme owl-carousel wow fadeInUp">
                            <div class="testimonial__items ralt bgadd round16">
                                <div class="ratting mb-16 d-flex align-items-center gap-2">
                                    <i class="material-symbols-outlined ratting">
                                        star
                                    </i>
                                    <i class="material-symbols-outlined ratting">
                                        star
                                    </i>
                                    <i class="material-symbols-outlined ratting">
                                        star
                                    </i>
                                    <i class="material-symbols-outlined ratting">
                                        star
                                    </i>
                                    <i class="material-symbols-outlined ratting">
                                        star
                                    </i>
                                </div>
                                <p class="ptext3 inter fz-18 fw-400 mb-30">
                                    Thanks to the credit card marketplace website, I was able to find the perfect credit
                                    card to fit my needs. The website was easy to navigate and provided me with all the
                                    information
                                </p>
                                <div class="d-flex align-items-center gap-3">
                                    <div class="thumb">
                                        <img src="assets/img/testimonial/annetee.png" alt="annette">
                                    </div>
                                    <div class="cont">
                                        <span class="fz-20 fw-600 inter ptext">
                                            Annette Black
                                        </span>
                                        <span class="fz-16 d-block ptext fw-400 inter">
                                            Canada
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="testimonial__items ralt bgadd round16">
                                <div class="ratting mb-16 d-flex align-items-center gap-2">
                                    <i class="material-symbols-outlined ratting">
                                        star
                                    </i>
                                    <i class="material-symbols-outlined ratting">
                                        star
                                    </i>
                                    <i class="material-symbols-outlined ratting">
                                        star
                                    </i>
                                    <i class="material-symbols-outlined ratting">
                                        star
                                    </i>
                                    <i class="material-symbols-outlined ratting">
                                        star
                                    </i>
                                </div>
                                <p class="ptext3 inter fz-18 fw-400 mb-30">
                                    Thanks to the credit card marketplace website, I was able to find the perfect credit
                                    card to fit my needs. The website was easy to navigate and provided me with all the
                                    information
                                </p>
                                <div class="d-flex align-items-center gap-3">
                                    <div class="thumb">
                                        <img src="assets/img/testimonial/annetee.png" alt="annette">
                                    </div>
                                    <div class="cont">
                                        <span class="fz-20 fw-600 inter ptext">
                                            Annette Black
                                        </span>
                                        <span class="fz-16 d-block ptext fw-400 inter">
                                            Canada
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="testimonial__items ralt bgadd round16">
                                <div class="ratting mb-16 d-flex align-items-center gap-2">
                                    <i class="material-symbols-outlined ratting">
                                        star
                                    </i>
                                    <i class="material-symbols-outlined ratting">
                                        star
                                    </i>
                                    <i class="material-symbols-outlined ratting">
                                        star
                                    </i>
                                    <i class="material-symbols-outlined ratting">
                                        star
                                    </i>
                                    <i class="material-symbols-outlined ratting">
                                        star
                                    </i>
                                </div>
                                <p class="ptext3 inter fz-18 fw-400 mb-30">
                                    Thanks to the credit card marketplace website, I was able to find the perfect credit
                                    card to fit my needs. The website was easy to navigate and provided me with all the
                                    information
                                </p>
                                <div class="d-flex align-items-center gap-3">
                                    <div class="thumb">
                                        <img src="assets/img/testimonial/annetee.png" alt="annette">
                                    </div>
                                    <div class="cont">
                                        <span class="fz-20 fw-600 inter ptext">
                                            Annette Black
                                        </span>
                                        <span class="fz-16 d-block ptext fw-400 inter">
                                            Canada
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
