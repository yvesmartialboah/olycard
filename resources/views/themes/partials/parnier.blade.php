<!-- Cart Section Here -->
<section class="cart__section pt-120 pb-120">
    <div class="container">
        <div class="cart__wrapper bgwhite round16">
            <div class="table__responsive">
                <table class="table align-baseline mb-24">
                    <thead>
                        <tr>
                            <th class="fz-18 fw-500 inter title">Carte</th>
                            <th class="fz-18 fw-500 inter title">Prix Unitaire</th>
                            <th class="fz-18 fw-500 inter title">Quanté</th>
                            <th class="fz-18 fw-500 inter title">Sous Total</th>
                            <th class="fz-18 fw-500 inter title">Supprimer</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <span class="max-240 d-flex align-items-center gap-2">
                                    <span class="smallicon">
                                        {{-- <img src="assets/img/cards/paysmall.png" alt="img"> --}}
                                        <img src="{{ asset('assets/img/cards/card4.png') }}" alt="card"
                                            class="choose__mthumb w-100">
                                    </span>
                                    <span class="fz-16 fw-400 inter title">
                                        Carte de crédit
                                    </span>
                                </span>
                            </td>
                            <td class="fz-16 fw-400 inter base">150000F</td>
                            <td>
                                <div class="quantity__box justify-content-center d-flex align-items-center">
                                    <input type="number" name="qty" id="qty" min="1" max="10"
                                        step="1" value="1">
                                    <div class="d-grid">
                                        <button class="qtyminus rotates" aria-hidden="true">
                                            <i class="material-symbols-outlined">
                                                keyboard_arrow_down
                                            </i>
                                        </button>
                                        <button class="qtyplus" aria-hidden="true">
                                            <i class="material-symbols-outlined">
                                                keyboard_arrow_down
                                            </i>
                                        </button>
                                    </div>
                                </div>
                            </td>
                            <td class="fz-16 fw-400 inter base">30000F</td>
                            <td>
                                <i class="material-symbols-outlined base">
                                    close
                                </i>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="promot__coded flex-wrap mb-40 gap-2 d-flex align-items-center justify-content-between">
                <form action="#" class="d-flex align-items-center jus
            ">
                    <input type="text" placeholder="Promo code">
                    <button class="cmn--btn">
                        <span>
                            Appliquer
                        </span>
                    </button>
                </form>
                {{-- <a href="#0" class="cmn--btn">
                    <span>
                        Update Cart
                    </span>
                </a> --}}
            </div>
            <div class="cart__totalwrap round16">
                <h5 class="title mb-30">
                    Cart Total
                </h5>
                <ul class="subtotal__list mb-30">
                    <li class="d-flex align-items-center">
                        <span class="fz-18 fw-500 inter title">
                            Sous total
                        </span>
                        <span class="fz-18 tbg fw-400 inter title">
                            $110.00
                        </span>
                    </li>
                    <li class="d-flex align-items-center">
                        <span class="fz-18 fw-500 inter title">
                            TVA
                        </span>
                        <span class="fz-18 tbg fw-400 inter title">
                            $15
                        </span>
                    </li>
                    <li class="d-flex align-items-center">
                        <span class="fz-18 fw-500 inter title">
                            Total
                        </span>
                        <span class="fz-18 tbg fw-400 inter base">
                            $125.00
                        </span>
                    </li>
                </ul>
                <a href="checkout.html" class="cmn--btn">
                    <span>
                        Valider
                    </span>
                </a>
            </div>
        </div>
    </div>
</section>
<!-- Cart Section End -->
