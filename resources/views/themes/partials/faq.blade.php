    <section class="faq__section pb-120 pt-120">
        <div class="container">
            <div class="row g-4 align-items-center justify-content-between">
                <div class="col-xl-6 col-lg-7">
                    <div class="app__content">
                        <div class="section__title mb-40">
                            <h4 class="sub ralt base mb-16 wow fadeInUp">
                                Frequently Asked Questions
                            </h4>
                            <h2 class="title mb-24 wow fadeInDown">
                                Find the Answers to Your Credit Card Questions Here
                            </h2>
                            <p class="ptext2 fz-16 fw-400 inter wow fadeInUp">
                                It refers to a list of common questions and answers related to a particular topic or
                                product. In the case of a credit card marketplace website
                            </p>
                        </div>
                        <div class="accordion__wrap">
                            <div class="accordion" id="accordionExample">
                                <!--Accordion items-->
                                <div class="accordion-item wow fadeInDown" data-wow-duration="0.7s">
                                    <div class="accordion-header" id="headingTwo">
                                        <button class="accordion-button collapsed" type="button"
                                            data-bs-toggle="collapse" data-bs-target="#collapseTwo"
                                            aria-expanded="false" aria-controls="collapseTwo">
                                            How do I apply for a credit card on the marketplace?
                                        </button>
                                        <div id="collapseTwo" class="accordion-collapse collapse"
                                            aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                            <div class="accordion-body">
                                                <p>
                                                    It refers to a list of common questions and answers related to a
                                                    particular topic or product. In the case of a credit card
                                                    marketplace website
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--Accordion items-->
                                <div class="accordion-item wow fadeInDown" data-wow-duration="0.9s">
                                    <h2 class="accordion-header" id="headingOne">
                                        <button class="accordion-button collapsed" type="button"
                                            data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true"
                                            aria-controls="collapseOne">
                                            How does the credit card marketplace work?
                                        </button>
                                    </h2>
                                    <div id="collapseOne" class="accordion-collapse collapse"
                                        aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                        <div class="accordion-body">
                                            <p>
                                                It refers to a list of common questions and answers related to a
                                                particular topic or product. In the case of a credit card marketplace
                                                website
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!--Accordion items-->
                                <div class="accordion-item wow fadeInDown" data-wow-duration="1s">
                                    <h2 class="accordion-header" id="headingThree">
                                        <button class="accordion-button collapsed" type="button"
                                            data-bs-toggle="collapse" data-bs-target="#collapseThree"
                                            aria-expanded="false" aria-controls="collapseThree">
                                            How can I improve my credit score?
                                        </button>
                                    </h2>
                                    <div id="collapseThree" class="accordion-collapse collapse"
                                        aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                        <div class="accordion-body">
                                            <p>
                                                It refers to a list of common questions and answers related to a
                                                particular topic or product. In the case of a credit card marketplace
                                                website
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!--Accordion items-->
                                <div class="accordion-item wow fadeInDown" data-wow-duration="1.4s">
                                    <h2 class="accordion-header" id="headingThree4">
                                        <button class="accordion-button collapsed" type="button"
                                            data-bs-toggle="collapse" data-bs-target="#collapseThree4"
                                            aria-expanded="false" aria-controls="collapseThree">
                                            What skills do I need to work in AI and ML?
                                        </button>
                                    </h2>
                                    <div id="collapseThree4" class="accordion-collapse collapse"
                                        aria-labelledby="headingThree4" data-bs-parent="#accordionExample">
                                        <div class="accordion-body">
                                            <p>
                                                It refers to a list of common questions and answers related to a
                                                particular topic or product. In the case of a credit card marketplace
                                                website
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!--Accordion items-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-5 col-lg-5">
                    <div class="app__thumb ralt">
                        <img src="assets/img/faq/faq.png" alt="card" class="w-100">
                    </div>
                </div>
            </div>
        </div>
    </section>
