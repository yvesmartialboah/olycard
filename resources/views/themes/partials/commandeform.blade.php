@push('css')
    <style>
        #formulaire {
            float: left;
            width: 50%;
            padding: 20px;
        }

        #carte {
            float: left;
            width: 50%;
            padding: 20px;
        }

        #carte-de-visite {
            max-width: 100%;
            height: auto;
        }
    </style>
@endpush

<!-- signup section Here -->
<section class="signup__section pt-120 pb-120">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-lg-6">
                <div class="signup__boxes round16">
                    <h3 class="title mb-16">
                        Informations complementaires
                    </h3>
                    <p class="fz-16 title fw-400 inter mb-40">

                    </p>
                    <form id="formulaire" action="#0" class="write__review">
                        <div class="row g-4 ">
                            <div class="col-lg-12">
                                <div class="frm__grp">
                                    {{-- <label for="enamee" class="fz-18 fw-500 inter title mb-16">Adresse Email ou numéro
                                        de téléphone</label> --}}
                                    <input type="text" id="enamee" placeholder="Entrez votre adresse email...">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="frm__grp">
                                    {{-- <label for="pas" class="fz-18 fw-500 inter title mb-16">Nom
                                    </label> --}}
                                    <input type="text" id="pas" placeholder="Nom">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="frm__grp">
                                    <input type="text" id="pas" placeholder="Prenoms">
                                    {{-- <a href="#" class="base fz-14 inter d-flex justify-content-end mt-2">Mot de
                                        passe oublié</a> --}}
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="frm__grp">
                                    <input type="text" id="pas" placeholder="Société">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="frm__grp">
                                    <input type="text" id="pas" placeholder="Adresse complémentaire">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="frm__grp">
                                    <Select class="form-control">
                                        <option value="1">Côte d'Ivoire</option>
                                    </Select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="frm__grp">
                                    <Select class="form-control">
                                        <option value="1">Ville</option>
                                    </Select>
                                </div>
                            </div>

                            {{-- <p class="fz-16 fw-400 title inter">
                                Avez-vous un compte? <a href="signup.html" class="base">S'inscrire</a>
                            </p> --}}
                            <div class="col-lg-12 block">
                                <div class="frm__grp">
                                    <button type="button" onclick="updateCarte() class="cmn--btn">
                                        <span>
                                            Vslider
                                        </span>
                                    </button>

                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-lg-6" id="carte">
                <div class="signup__thumb">
                    <img id="carte-de-visite" alt="Carte de visite"
                        src="https://www.visa.ca/dam/VCOM/regional/na/canada/pay-with-visa/cards/credit/visa-gold-recto-800x450.jpg"
                        class="w-100">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- signup section End -->

@push('js')
    <script>
        $(document).ready(function() {
            // Votre code JavaScript ici
            function updateCarte() {
                var nom = $("#nom").val();
                var prenom = $("#prenom").val();
                var entreprise = $("#entreprise").val();

                var carteDeVisite = $("#carte-de-visite");
                carteDeVisite.attr("src",
                    "https://www.visa.ca/dam/VCOM/regional/na/canada/pay-with-visa/cards/credit/visa-gold-recto-800x450.jpg"
                    );

                var nomElement = $("<p>").text("Nom: " + nom);
                carteDeVisite.after(nomElement);

                var prenomElement = $("<p>").text("Prénom: " + prenom);
                carteDeVisite.after(prenomElement);

                var entrepriseElement = $("<p>").text("Entreprise: " + entreprise);
                carteDeVisite.after(entrepriseElement);
            }

            $("#updateButton").click(updateCarte);
        });
    </script>
@endpush
