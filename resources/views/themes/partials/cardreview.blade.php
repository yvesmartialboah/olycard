    <section class="card__rated ralt pb-120">
        <div class="container">
            <div class="row justify-content-center mb-60">
                <div class="col-lg-6 col-md-9">
                    <div class="section__title text-center">
                        <h4 class="sub ralt base mb-16 wow fadeInDown">
                            Top Card Reviews
                        </h4>
                        <h2 class="title wow fadeInUp">
                            Get ready to buy our unique credit card
                        </h2>
                        <p class="ptext2 fz-16 fw-400 inter wow fadeInDown">
                            Buying a credit card from a credit card marketplace website involves browsing, comparing,
                            selecting
                        </p>
                    </div>
                </div>
            </div>
            <div class="row g-4 justify-content-center">
                <div class="col-xl-4 col-lg-4 col-md-6 wow fadeInUp" data-wow-duration="1.2s">
                    <div class="top__items bgwhite round16 bgwhite">
                        <div class="icon m-auto d-flex align-items-center base justify-content-center boxes4 round50">
                            <i class="material-symbols-outlined">
                                social_leaderboard
                            </i>
                        </div>
                        <div class="content text-center mt-24">
                            <h4 class="mb-16">
                                <a href="listing-details.html" class="title">
                                    Top Rewards Card
                                </a>
                            </h4>
                            <p class="fz-16 fw-400 inter ptext2 mb-30">
                                We understand that the world of credit cards can be overwhelming
                            </p>
                            <a href="listing-details.html" class="cmn--btn outline__btn">
                                <span>
                                    Maximize Your Rewards
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 wow fadeInUp" data-wow-duration="1.5s">
                    <div class="top__items bgwhite round16 bgwhite">
                        <div class="icon m-auto d-flex align-items-center justify-content-center boxes2 round50">
                            <i class="material-symbols-outlined icolor1">
                                social_leaderboard
                            </i>
                        </div>
                        <div class="content text-center mt-24">
                            <h4 class="mb-16">
                                <a href="listing-details.html" class="title">
                                    Best Card Of 2023
                                </a>
                            </h4>
                            <p class="fz-16 fw-400 inter ptext2 mb-30">
                                We understand that the world of credit cards can be overwhelming
                            </p>
                            <a href="listing-details.html" class="cmn--btn outline__btn">
                                <span>
                                    See The Best Cards
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 wow fadeInUp" data-wow-duration="1.7s">
                    <div class="top__items bgwhite round16 bgwhite">
                        <div class="icon m-auto d-flex align-items-center justify-content-center boxes3 round50">
                            <i class="material-symbols-outlined icolor2">
                                redeem
                            </i>
                        </div>
                        <div class="content text-center mt-24">
                            <h4 class="mb-16">
                                <a href="listing-details.html" class="title">
                                    Best cards for bad credit
                                </a>
                            </h4>
                            <p class="fz-16 fw-400 inter ptext2 mb-30">
                                We understand that the world of credit cards can be overwhelming
                            </p>
                            <a href="listing-details.html" class="cmn--btn outline__btn">
                                <span>
                                    Our Top Picks
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
