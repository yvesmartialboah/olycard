    <section class="choose__section ralt bgadd pb-120 pt-120">
        <div class="container">
            <div class="row g-4 align-items-center">
                <div class="col-xl-6 col-lg-6">
                    <div class="choose__thumb ralt">
                        <img src="{{ asset('assets/img/cards/card4.png') }}" alt="card" class="choose__mthumb w-100">
                        <img src="{{ asset('assets/img/work/map.png') }}" alt="map-img" class="mapthumb">
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6">
                    <div class="choose__content">
                        <div class="section__title mb-40">
                            <h4 class="sub ralt base mb-16 wow fadeInUp" data-wow-duration="1.1s">

                            </h4>
                            <h2 class="title mb-24 wow fadeInUp" data-wow-duration="1.2s">
                                Carte standard
                            </h2>
                        </div>
                        <div class="marketplace__user pt-40 d-flex align-items-center flex-wrap wow fadeInDown">

                            <a href="{{ route('users.commandeform') }}" class="cmn--btn" style="margin: 10px;">
                                <span>
                                    Commander ma carte
                                </span>
                            </a>
                            <button type="button" data-bs-toggle="modal" data-bs-target="#rdvModal"
                                href="{{ route('card.personnaliser') }}" class="cmn--btn" style="margin: 10px;"
                                type="button">
                                <span>
                                    Personnaliser ma carte
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <h2 class="title mb-24 wow fadeInUp" data-wow-duration="1.2s">
                    Avantages:
                </h2>
                <ul class="text-dark">
                    <li>
                        Analyse des interactions : Suivi des interactions avec la carte, comme le nombre de scans ou
                        les
                        clics sur les liens partagés.
                    <li>
                        Liens vers les profils en ligne : URL vers les réseaux sociaux, site web ou autres plateformes
                        professionnelles.
                    </li>
                    <li>
                        Partage via QR Code ou NFC : Possibilité de partager les informations via ces méthodes.
                    </li>
                    <li>
                        Personnalisation basique : Choisir parmi des modèles prédéfinis ou des options de
                        personnalisation limitées.
                    </li>
                    <li>
                        Enregistrement sur le téléphone : Permet à l'utilisateur de sauvegarder les informations
                        directement dans les contacts du téléphone.
                    </li>
                    </li>
                </ul>
            </div>
        </div>

    </section>
