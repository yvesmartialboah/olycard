<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from brandio.io/envato/iofrm/html/login2.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 11 Feb 2024 23:36:43 GMT -->

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>iofrm</title>
    <title>{{ config('app.name') }} - {{ $title ?? '' }}</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_admin/auth/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_admin/auth/css/fontawesome-all.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_admin/auth/css/iofrm-style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_admin/auth/css/iofrm-theme2.css') }}">
</head>

<body>
    <div class="form-body">
        <div class="website-logo">
            <a href="index.html">
                <div class="logo">
                    <img class="logo-size" src="images/logo-light.svg" alt="">
                </div>
            </a>
        </div>
        <div class="row">
            <div class="img-holder">
                <div class="bg"></div>
                <div class="info-holder">

                </div>
            </div>
            <div class="form-holder">
                <div class="form-content">
                    <div class="form-items">
                        <h3>Connexion Administrateur</h3>
                        {{-- <p>Access to the most powerfull tool in the entire design and web industry.</p> --}}
                        {{-- <div class="page-links">
                            <a href="login2.html" class="active">Login</a><a href="register2.html">Register</a>
                        </div> --}}
                        <form role="form" method="POST" action="{{ url('/admin/register') }}">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <input class="form-control" type="text" name="name" placeholder="Nom" required>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <input class="form-control" type="text" name="email" placeholder="E-mail Address"
                                    required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>


                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <input class="form-control" type="password" name="password" placeholder="Mot de passe"
                                    required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <input class="form-control" type="password" name="password_confirmation"
                                    placeholder="confirmation" required>
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong
                                            class="text-danger">{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                            {{-- <input type="checkbox" id="chk1"><label for="chk1">Restez connecté</label> --}}
                            <div class="form-button">
                                <button id="submit" type="submit" class="ibtn">Connexion</button> <a
                                    href="forget2.html">Mot depasse oublité?</a>
                            </div>
                        </form>
                        <div class="other-links">
                            <span>Copyright {{ date('Y') }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('asset_admin/auth/js/jquery.min.js') }}"></script>
    <script src="{{ asset('asset_admin/auth/js/popper.min.js') }}"></script>
    <script src="{{ asset('asset_admin/auth/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('asset_admin/auth/js/main.js') }}"></script>
</body>

<!-- Mirrored from brandio.io/envato/iofrm/html/login2.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 11 Feb 2024 23:36:46 GMT -->

</html>
