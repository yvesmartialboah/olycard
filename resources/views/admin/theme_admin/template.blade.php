@include('admin.theme_admin.layouts.head')
<section class="main_content dashboard_part large_header_bg">
    @include('admin.theme_admin.layouts.sidebae')
    @include('admin.theme_admin.layouts.header')
    <div class="main_content_iner default_main_contaner_iner">
        <div class="container-fluid p-0 ">

            @yield('content')
        </div>
    </div>
    @include('admin.theme_admin.layouts.footer')
</section>
