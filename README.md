# KNOW PROJECT 

## ABOUT OLYCARD

[Generation_du_client_token] : php artisan passport:client --password

## commande pour démarrer le projet

* php artisan migrate:refresh
* php artisan db:seed
* php artisan serve
* php artisan make:model Models/nom_du_model -m
* php artisan make:controller nom_du_Controller --model=Models/nom_du_model --api
* php artisan make:factory nom_factory 
* php artisan make:seed nom_seed 

### Debug

* - Generate new key : php artisan key:generate
* - Clear the config : php artisan config:clear
* - Update cache php : artisan config:cache

## NB Currently Features

***Install Dep***
- activate extension gd in php.ini
* composer update --ignore-platform-req=ext-gd
* composer update --ignore-platform-req=ext-gd --ignore-platform-req=ext-fileinfo

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

