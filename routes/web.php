<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SiteController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\AdminAuth\LoginController;
use App\Http\Controllers\GenerateIdentityController;
use App\Http\Controllers\AdminAuth\RegisterController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Accueil
Route::get('/',[SiteController::class,'index'])->name('home');
// Enregistrer un RDV
Route::post('/rdv-store',[SiteController::class,'storeRendezvous']);

//Nos offres
Route::get('/nos-offres',[SiteController::class,'offres'])->name('offres');
//Nos login
Route::get('/login-users',[SiteController::class,'loginForm'])->name('login');
//register
Route::get('/inscription',[SiteController::class,'registerForm'])->name('register');
//register user
Route::post('insction-user',[SiteController::class,'store'])->name('users.store');
//commande
Route::get('/personnaliser-carte',[SiteController::class,'personnaliserCard'])->name('card.personnaliser')->middleware('auth');
//connexion
Route::post('/conexion',[SiteController::class,'login'])->name('users.login');
//deconnexion
Route::get('/deconexion',[SiteController::class,'logout'])->name('users.logout');
//checkout
Route::get('/checkout',[SiteController::class,'checkout'])->name('users.checkout')->middleware('auth');
//Formulaire d'information complementaire avant la commande
Route::get('/informations',[SiteController::class,'cammandeForm'])->name('users.commandeform')->middleware('auth');

Route::resource('user', GenerateIdentityController::class);
Route::get('identity/{slug?}', [GenerateIdentityController::class, 'showSlug'])->name('users.identity');
Route::get('qrcode', [GenerateIdentityController::class, 'testQrCode'])->name('users.qrCode');

//Route
Route::group(['prefix' => 'admin'], function () {
  Route::get('/login', [LoginController::class,'showLoginForm'])->name('loginAdmin');
  Route::post('/login', [LoginController::class,'login']);
  Route::get('/logout', [LoginController::class,'deconnexion'])->name('logout')->middleware('admin');
//   Route::post('/logout', [LoginController::class,'logout'])->name('logout');

  Route::get('/register', [RegisterController::class,'showRegistrationForm'])->name('registerAdmin');
  Route::post('/register', [RegisterController::class,'register']);

  Route::get('/home',[DashboardController::class,'index']);

//   Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
//   Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset')->name('password.email');
//   Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
//   Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
});
