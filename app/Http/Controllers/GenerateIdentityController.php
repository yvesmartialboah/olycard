<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

use \Contacts\Options; // Only needed if you must override the default settings
use \Contacts\Vcard;

class GenerateIdentityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return view('themes_portefolio.user_portefolio.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showSlug($slug = null)
    {
        $user = User::where('slug', $slug)->first();
        // Informations VCard
        $vCardData = [
            'firstName' => $user->name,
            'lastName' => $user->prenom,
            'phone' => $user->contact_1,
            'mobile' => $user->contact_2,
            'address' => $user->localisation_name,
            'email' => $user->email,
            'company' => $user->entreprise,
            'job_title' => $user->fonction,
            'website' => $user->site_internet ?? $user->link_linkedin ?? $user->link_facebook,
            'note' => $user->description,
            'birthday' => $user->date_de_naissance,
            'social_profiles' => [
                'linkedin' => $user->link_linkedin,
                'website' => $user->site_interne,
                'facebook' => $user->link_facebook
            ],
            'image' => $user->image_de_profil,
        ];
        // Générer le contenu VCard
        $vCardContent = "BEGIN:VCARD\n"
        . "VERSION:3.0\n"
        . "FN:{$vCardData['firstName']} {$vCardData['lastName']}\n"
        . "TEL:{$vCardData['phone']}\n"
        . "TEL;TYPE=CELL:{$vCardData['mobile']}\n"
        . "EMAIL:{$vCardData['email']}\n"
        . "ADR:{$vCardData['address']}\n"
        . "ORG:{$vCardData['company']}\n"
        . "TITLE:{$vCardData['job_title']}\n"
        . "URL:{$vCardData['website']}\n"
        . "NOTE:{$vCardData['note']}\n"
        . "BDAY:{$vCardData['birthday']}\n";

        // Ajouter des profils sociaux
        if (!empty($vCardData['socialProfiles'])) {
            foreach ($vCardData['socialProfiles'] as $platform => $url) {
                $vCardContent .= "X-SOCIALPROFILE;TYPE={$platform}:$url\n";
            }
        }

        // Ajouter une image
        if (!empty($vCardData['image'])) {
            $vCardContent .= "PHOTO;VALUE=URL;TYPE=PNG:{$vCardData['image']}\n";
        }

        $vCardContent .= "END:VCARD";

        // Chemin vers l'image que vous souhaitez superposer au centre du code QR
        $imagePath = public_path('assets/img/logoQrx.png');

        // Générer le code QR à partir du contenu VCard
        $qrCoder = QrCode::format('png')
            ->merge($imagePath, 0.25, true) // 0.5 est la taille de l'image relative au code QR
            ->size(300)
            ->generate($vCardContent);
        // Convertir l'image en Base64
        $qrCode = base64_encode($qrCoder);

        // Enregistrez le contenu du fichier vCard dans un fichier
        define('UPLOAD_DIR', 'ContactVCF/');
        $filename = $user->name.'_'.$user->id.'.vcf';
        $file = UPLOAD_DIR . $filename;
        file_put_contents($file, $vCardContent);

        if ($user)
            return view('themes_portefolio.user_portefolio.user', compact('user', 'qrCode', 'file'));
        else
            return null;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function testQrCode()
    {
        // Informations VCard
        $vCardData = [
            'firstName' => 'Oumar',
            'lastName' => 'kiemde',
            'phone' => '+2250504928652',
            'mobile' => '',
            'address' => "Abidjan, Commune d'anyanma",
            'email' => 'okiemde42@gmail.com',
            'company' => 'Oumar kiemde',
            // 'job_title' => 'Entrepreneur',
            'job_title' => 'Éleveur de Lapain et poulet et vente des boubous islamiques.',
            'website' => 'https://www.facebook.com/profile.php?id=100075709181717&mibextid=ZbWKwL',
            'note' => '',
            'birthday' => '',
            'social_profiles' => [
                'linkedin' => '',
                'twitter' => '',
                'facebook' => ''
            ],
            'image' => '',
        ];

        // Générer le contenu VCard
        $vCardContent = "BEGIN:VCARD\n"
        . "VERSION:3.0\n"
        . "FN:{$vCardData['firstName']} {$vCardData['lastName']}\n"
        . "TEL:{$vCardData['phone']}\n"
        . "TEL;TYPE=CELL:{$vCardData['mobile']}\n"
        . "EMAIL:{$vCardData['email']}\n"
        . "ADR:{$vCardData['address']}\n"
        . "ORG:{$vCardData['company']}\n"
        . "TITLE:{$vCardData['job_title']}\n"
        . "URL:{$vCardData['website']}\n"
        . "NOTE:{$vCardData['note']}\n"
        . "BDAY:{$vCardData['birthday']}\n";

        // Ajouter des profils sociaux
        if (!empty($vCardData['socialProfiles'])) {
            foreach ($vCardData['socialProfiles'] as $platform => $url) {
                $vCardContent .= "X-SOCIALPROFILE;TYPE={$platform}:$url\n";
            }
        }

        // Ajouter une image
        if (!empty($vCardData['image'])) {
            $vCardContent .= "PHOTO;VALUE=URL;TYPE=PNG:{$vCardData['image']}\n";
        }

        $vCardContent .= "END:VCARD";


        // Chemin vers l'image que vous souhaitez superposer au centre du code QR
        $imagePath = public_path('assets/img/logoQrx.png');

        // Générer le code QR à partir du contenu VCard
        $qrCode = QrCode::format('png')
            ->merge($imagePath, 0.25, true) // 0.5 est la taille de l'image relative au code QR
            ->size(300)
            ->generate($vCardContent);

        // Sauvegarde de l'image sur le Serveur
        define('UPLOAD_DIR', 'imagesGenerateQrCodes/');
        $filename = uniqid() . '.png';
        $file = UPLOAD_DIR . $filename;
        file_put_contents($file, $qrCode);

        /*
            Sauvegarder le nom de l'image
        */

        // Retourner l'image du code QR en réponse
        return Response::make($qrCode, 200, ['Content-Type' => 'image/png']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
