<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
   public function __construct() {
    $this->middleware('admin');
   }

    // page d'acueil
    public function index(){
        //Variables
        $data['title'] = "Accueil";
        $data['menu'] = "accueil";

        return view('admin.theme_admin.home',$data);
    }

    //Deconnexion
    public function deconnexion(){
        auth('admin')->logout();
        return back();
    }
}
