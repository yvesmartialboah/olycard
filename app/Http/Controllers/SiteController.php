<?php
/**
 * Ce fichier contient la classe SiteController.
 * Il s'agit d'un contrôleur pour la gestion des pages du site.
 */
namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Rendezvous;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class SiteController extends Controller
{
    /**
     * Affiche la page d'accueil.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $data['title'] = "Accueil";
        $data['menu'] = "accueil";
        return view('themes.siteweb.home', $data);
    }

    /**
     * Affiche la page des offres.
     *
     * @return \Illuminate\View\View
     */
    public function offres()
    {
        $data['title'] = "Offres";
        $data['menu'] = "offres";
        return view('themes.siteweb.offres', $data);
    }

    public function loginForm(){
        $data['title'] = "Connexion";
        $data['menu'] = "login";
        return view('auth.loginpage', $data);
    }

    public function registerForm(){
        $data['title'] = "Inscription";
        $data['menu'] = "register";
        return view('auth.registerpage', $data);
    }

    public function store(Request $request){
        $this->validate($request,[
            'email' => 'required',
            'contact_1' => 'required|unique:users',
            'name' => 'required',
            'prenom' => 'required',
            'password' => 'required|confirmed|min:7',
        ],[
            'email' => 'email obligatoire',
            // 'contact_1.min:10' => 'le le numéro de téléphone doit 10 carateres minimum',
            // 'contact_1.max:14' => 'le numéro de téléphone doit contenir 14 carateres maximum',
            'contact_1.required' =>'telephone obligatoire',
            'contact_1.unique' =>'telephone existe déjà',
            'name' => 'nom obligatoire',
            'prenom' => 'prenom obligatoire',
            'password.confirmed' => 'les mots de passe ne correspondent pas',
            'password.required' => 'mot de passe obligatoire',
            'password.min:7' => 'le mot de passe doit contenir 7 carateres minimum',

        ]);

        $data = [
            'email' => $request->email,
            'contact_1' => $request->contact_1,
            'name' => $request->name,
            'prenom' => $request->prenom,
            'slug'=> time().Str::slug($request->prenom),
            'entreprise' => $request->entreprise,
            'password' => Hash::make($request->password),
        ];

        if(!User::create($data)){
            session()->flash('type','alert-danger');
            session()->flash('message','Une erreur s\'est produite, lors de la création de votre compte.');
            return back();
        }else{
            if(auth('web')->attempt(['email' => $request->email, 'password' => $request->password])){

                session()->flash('type','alert-success');
                session()->flash('message','Inscription réussie.');

                return redirect()->route('users.identity');
            }

        }


    }

    public function personnaliserCard(){
        $data['title'] = "Personnaliser votre carte";
        $data['menu'] = "personnaliser";
        return view('themes.siteweb.commande', $data);
    }

    public function login(Request $request){
        $this->validate($request,[
            'email'=>'required',
            'password'=>'required',
        ]);
        // dd(auth()->attempt(['email' => 'dev@gmail.com', 'password' => $request->password]));

        if(auth('web')->attempt(['email' => $request->email, 'password' => $request->password])){
            session()->flash('type','alert-success');
            session()->flash('message','connexion réusie');
            return redirect()->route('users.identity');
        }else{
            session()->flash('type','alert-danger');
            session()->flash('message','Email ou mot de passe incorrecte');
            return back();
        }
    }

    public function checkout(Request $request){
        $data['title'] = "Checkout";
        $data['menu'] = "parnier";
        return view('themes.siteweb.checkout',$data);
    }

    public function cammandeForm(Request $request){
        $data['title'] = "Commande";
        $data['menu'] = "Commande";
        return view('themes.siteweb.commande',$data);
    }

public function storeRendezvous(Request $request){

        $this->validate($request, [
            'nom' => 'required',
            'prenom' => 'required',
            'email' => 'required',
            'telephone' => 'required',
        ]

    );


$rendezvous = Rendezvous::create([
    'reference' => str_random(9),
    'nom' => $request->nom,
    'prenom' => $request->prenom,
    'email' => $request->email,
    'telephone' => $request->telephone,
    'daterendezvous' => $request->daterendezvous,
    'statut' =>  0,
]);

// a envoyer  a la vue vue de l'mail
$data['nom'] = $request->nom;
$data['prenom'] = $request->prenom;
$data['email'] = $request->email;
$data['telephone'] = $request->telephone;
$data['daterendezvous'] = $request->daterendezvous;
$data['reference'] = $rendezvous->reference;

@Mail::send('emails.rendezvous', $data, function ($message) use ($data) {
    $message->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))->to($data['email'])->subject('Confirmation de rendez-vous');
});



}

    public function logout(){
        auth()->logout();
        return back();
    }
}
