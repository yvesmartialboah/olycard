<?php

namespace App\Models;

use App\Models\Commande;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Rendezvous extends Model
{
    use HasFactory;
    protected $fillable = ['reference','daterendezvous','nom','prenom','statut','telephone','email'];

    public function commande(){
        return $this->belongsTo(Commande::class);
    }

}
