<?php

namespace App\Models;

use App\Models\Pays;
use App\Models\User;
use App\Models\Ville;
use App\Models\Rendezvous;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Commande extends Model
{
    use HasFactory;
    protected $fillable = [
        'libelle',
        'societe',
        'adressecoplementaire',
        'quantite',
        'datelivraison',
        'statut',
        'pays_id',
        'ville_id',
        'user_id',
    ];

    public function pays(){
        return $this->belongsTo(Pays::class);
    }
    public function ville(){
        return $this->belongsTo(Ville::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function rendezvous(){
        return $this->hasMany(Rendezvous::class);
    }
}
